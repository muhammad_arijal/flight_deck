# Compiled with Qt4

QT += core gui declarative widgets
CONFIG += serialport

TARGET = Flight_Deck
TEMPLATE = app
UI_DIR = .ui
MOC_DIR = .moc
OBJECTS_DIR = .obj
DESTDIR = $$PWD

win32: {
    RC_FILE = resources/Flight_Deck.rc
}

RESOURCES = resources/Resources.qrc

DEFINES += PROJECT_PATH=\"\\\"$$PWD\\\"\"

#################
# Main Source
#################
SOURCES +=  src/main.cpp
SOURCES +=  Task_Serial_Communication.cpp
HEADERS +=  Task_Serial_Communication.h

#################
# UI Class
#################
include($$PWD/libs/Altitude_Meter_Display/Altitude_Meter_Display.pri)
include($$PWD/libs/Battery_Status_Display/Battery_Status_Display.pri)
include($$PWD/libs/Compass_Display/Compass_Display.pri)
include($$PWD/libs/Main_Window/Main_Window.pri)
include($$PWD/libs/File_Dialog/File_Dialog.pri)
include($$PWD/libs/Open_Street_Map/Open_Street_Map.pri)
include($$PWD/libs/Speedometer_Display/Speedometer_Display.pri)
include($$PWD/libs/Vertical_Speed_Indicator_Display/Vertical_Speed_Indicator_Display.pri)
include($$PWD/libs/Terminal_Console/Terminal_Console.pri)
include($$PWD/libs/Joystick_Display/Joystick_Display.pri)
include($$PWD/libs/Media_Player/Media_Player.pri)

###########
# Marble
###########
win32 {
    INCLUDEPATH += $$quote(C:/Program Files (x86)/marble/include)
    LIBS += -L$$quote(C:/Program Files (x86)/marble/)
    CONFIG(release, debug|release): {
        LIBS += -llibmarblewidget
    }

    CONFIG(debug, debug|release): {
        LIBS += -llibmarblewidgetd
    }
}
unix {
    LIBS += -L/usr/local/lib \
            -lmarblewidget
}

######
# SFML
######
win32: {
    CONFIG(release, debug|release): LIBS += -lsfml-main -lsfml-audio -lsfml-graphics -lsfml-network -lsfml-window -lsfml-system
    CONFIG(debug, debug|release): LIBS += -lsfml-main-d -lsfml-audio-d -lsfml-graphics-d -lsfml-network-d -lsfml-window-d -lsfml-system-d
    INCLUDEPATH += C:\Shared_Libs\SFML-2.1\include
    DEPENDPATH += C:\Shared_Libs\SFML-2.1\include
    LIBS += -LC:\Shared_Libs\SFML-2.1\bin \
            -LC:\Shared_Libs\SFML-2.1\lib
}
unix: {

    INCLUDEPATH += /usr/include/SFML
    LIBS += -L/usr/lib -lsfml-audio -lsfml-graphics -lsfml-network -lsfml-window -lsfml-system
}

###########
# QWT
###########
win32 {

}
unix {
    CONFIG += qwt
    LIBS += -lqwt
}

###########
# VLC
###########
win32 {

}
unix {
    LIBS += -lvlc
}
