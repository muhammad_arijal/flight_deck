#include "Task_Serial_Communication.h"

Task_Serial_Communication::Task_Serial_Communication(QObject *parent) : QThread(parent)
{

}

void Task_Serial_Communication::run()
{
    QMutex mutex;
    while(!stop_flag)
    {
//        if(!udp_socket_client->Get_Message().isEmpty())
//        {
//            udp_socket_client->Client_Send(udp_socket_client->Get_Message());
//            udp_socket_client->Set_Message("");
//        }
        mutex.lock();
        if (stop_flag)
        {
            break;
        }
        mutex.unlock();
    }
}

void Task_Serial_Communication::stop(bool mode)
{
    stop_flag = mode;
}
