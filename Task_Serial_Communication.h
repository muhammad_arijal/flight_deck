#ifndef TASK_SERIAL_COMMUNICATION_H
#define TASK_SERIAL_COMMUNICATION_H

#include <QThread>
#include <QMutex>

class Task_Serial_Communication : public QThread
{
    Q_OBJECT

    public:
        explicit Task_Serial_Communication(QObject *parent = 0);
        void run();
        void stop(bool mode);

    signals:
    
    private:
        bool stop_flag;
};

#endif // TASK_SERIAL_COMMUNICATION_H
