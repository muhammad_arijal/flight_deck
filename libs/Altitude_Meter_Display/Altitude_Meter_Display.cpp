#include "Altitude_Meter_Display.h"

Altitude_Meter_Display::Altitude_Meter_Display(QWidget *parent) : QWidget(parent)
{
    QUrl url;

    altitude_meter_view = new QDeclarativeView();
    widget = new QWidget(this);
    horizontal_slider = new QSlider(widget);
    layout = new QVBoxLayout(widget);

    url.setUrl("qrc:/qml/qml/Altitude_Meter/main.qml");
    altitude_meter_view->setSource(url);

    horizontal_slider->setOrientation(Qt::Horizontal);
    horizontal_slider->setMinimum(MIN_VALUE);
    horizontal_slider->setMaximum(MAX_VALUE);
    horizontal_slider->setValue(INIT_VALUE);

    layout->addWidget(altitude_meter_view);
    layout->addWidget(horizontal_slider);

    root_object = altitude_meter_view->rootObject();

    widget->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    setMinimumSize(200,230);
    setMaximumSize(200,230);
    setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    connect(horizontal_slider, SIGNAL(valueChanged(int)), this, SLOT(Set_Value(int)));
}

Altitude_Meter_Display::~Altitude_Meter_Display()
{
    delete root_object;
    delete layout;
    delete altitude_meter_view;
    delete horizontal_slider;
    delete widget;
}

void Altitude_Meter_Display::Set_Value(int value)
{
    root_object->setProperty("value",value);
}

