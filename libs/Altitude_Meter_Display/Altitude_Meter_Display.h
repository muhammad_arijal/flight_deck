#ifndef ALTITUDE_METER_DISPLAY_H
#define ALTITUDE_METER_DISPLAY_H

#include <QGraphicsObject>
#include <QtDeclarative/QDeclarativeView>
#include <QLayout>
#include <QSlider>
#include <QWidget>

class Altitude_Meter_Display : public QWidget
{
    Q_OBJECT
    
    public:
        explicit Altitude_Meter_Display(QWidget *parent = 0);
        ~Altitude_Meter_Display();
    
    private slots:
        void Set_Value(int value);

    private:
        QDeclarativeView *altitude_meter_view;
        QGraphicsObject *root_object;
        QSlider *horizontal_slider;
        QVBoxLayout *layout;
        QWidget *widget;
        static const int MIN_VALUE = 0;
        static const int MAX_VALUE = 1000;
        static const int INIT_VALUE = MIN_VALUE;
};

#endif // ALTITUDE_METER_DISPLAY_H
