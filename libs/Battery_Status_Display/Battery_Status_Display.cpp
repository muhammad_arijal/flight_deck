#include "Battery_Status_Display.h"

Battery_Status_Display::Battery_Status_Display(QWidget *parent) : QWidget(parent)
{
    label_battery = new QLabel("Battery :",this);
    label_battery_value = new QLabel(QString::number(INIT_VALUE),this);
    label_percentage = new QLabel("%",this);
    battery_bar = new QProgressBar(this);

    widget = new QWidget(this);
    horizontal_slider = new QSlider(widget);
    text_layout = new QHBoxLayout();
    layout = new QVBoxLayout(widget);

    label_battery_value->setMinimumSize(18,0);
    label_battery_value->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Preferred);
    label_battery_value->setAlignment(Qt::AlignVCenter);

    battery_bar->setMinimumSize(330,0);
    battery_bar->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    battery_bar->setOrientation(Qt::Horizontal);
    battery_bar->setTextVisible(false);
    battery_bar->setMinimum(MIN_VALUE);
    battery_bar->setMaximum(MAX_VALUE);
    battery_bar->setValue(INIT_VALUE);

    text_layout->addWidget(label_battery);
    text_layout->addWidget(battery_bar);
    text_layout->addWidget(label_battery_value);
    text_layout->addWidget(label_percentage);

    horizontal_slider->setOrientation(Qt::Horizontal);
    horizontal_slider->setMinimum(MIN_VALUE);
    horizontal_slider->setMaximum(MAX_VALUE);
    horizontal_slider->setValue(INIT_VALUE);

    layout->addLayout(text_layout);
    layout->addWidget(horizontal_slider);

    setMinimumSize(450,50);
    setMaximumSize(450,50);
    setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    // Connect the zoom slider to the progress bar and vice versa
    connect(horizontal_slider, SIGNAL(valueChanged(int)), battery_bar, SLOT(setValue(int)));
    connect(horizontal_slider, SIGNAL(valueChanged(int)), label_battery_value, SLOT(setNum(int)));
}

Battery_Status_Display::~Battery_Status_Display()
{
    delete label_battery;
    delete label_battery_value;
    delete label_percentage;
    delete battery_bar;
    delete layout;
    delete horizontal_slider;
    delete widget;
}
