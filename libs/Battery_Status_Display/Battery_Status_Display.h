#ifndef BATTERY_STATUS_DISPLAY_H
#define BATTERY_STATUS_DISPLAY_H

#include <QLayout>
#include <QLabel>
#include <QProgressBar>
#include <QSlider>
#include <QWidget>

class Battery_Status_Display : public QWidget
{
    Q_OBJECT
    
    public:
        explicit Battery_Status_Display(QWidget *parent = 0);
        ~Battery_Status_Display();
    
    private:
        QLabel *label_battery;
        QLabel *label_battery_value;
        QLabel *label_percentage;
        QProgressBar *battery_bar;
        QSlider *horizontal_slider;
        QHBoxLayout *text_layout;
        QVBoxLayout *layout;
        QWidget *widget;
        static const int MIN_VALUE = 0;
        static const int MAX_VALUE = 100;
        static const int INIT_VALUE = MIN_VALUE;
};

#endif // BATTERY_STATUS_DISPLAY_H
