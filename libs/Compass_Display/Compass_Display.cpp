#include "Compass_Display.h"

Compass_Display::Compass_Display(QWidget *parent) : QWidget(parent)
{
    QUrl url;

    compass_view = new QDeclarativeView();
    widget = new QWidget(this);
    horizontal_slider = new QSlider(widget);
    layout = new QVBoxLayout(widget);

    url.setUrl("qrc:/qml/qml/Compass/main.qml");
    compass_view->setSource(url);

    horizontal_slider->setOrientation(Qt::Horizontal);
    horizontal_slider->setMinimum(MIN_VALUE);
    horizontal_slider->setMaximum(MAX_VALUE);
    horizontal_slider->setValue(INIT_VALUE);

    layout->addWidget(compass_view);
    layout->addWidget(horizontal_slider);

    root_object = compass_view->rootObject();

    widget->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    setMinimumSize(200,230);
    setMaximumSize(200,230);
    setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    connect(horizontal_slider, SIGNAL(valueChanged(int)), this, SLOT(Set_Value(int)));
}

Compass_Display::~Compass_Display()
{
    delete root_object;
    delete layout;
    delete compass_view;
    delete horizontal_slider;
    delete widget;
}

void Compass_Display::Set_Value(int value)
{
    root_object->setProperty("value",value);
}

