#ifndef COMPASS_DISPLAY_H
#define COMPASS_DISPLAY_H

#include <QGraphicsObject>
#include <QtDeclarative/QDeclarativeView>
#include <QLayout>
#include <QSlider>
#include <QWidget>

class Compass_Display : public QWidget
{
    Q_OBJECT
    
    public:
        explicit Compass_Display(QWidget *parent = 0);
        ~Compass_Display();

    private slots:
        void Set_Value(int value);

    private:
        QDeclarativeView *compass_view;
        QGraphicsObject *root_object;
        QSlider *horizontal_slider;
        QVBoxLayout *layout;
        QWidget *widget;
        static const int MIN_VALUE = 0;
        static const int MAX_VALUE = 3600;
        static const int INIT_VALUE = MIN_VALUE;
};

#endif // COMPASS_DISPLAY_H
