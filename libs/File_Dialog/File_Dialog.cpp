#include "File_Dialog.h"

File_Dialog::File_Dialog(QWidget *parent) : QWidget(parent)
{
    tree_view = new QTreeView(this);
    file_dialog_model = new QDirModel(this);
    button_open_save = new QPushButton("",this);
    button_cancel = new QPushButton("Cancel",this);
    label_file_name = new QLabel("File Name :",this);
    label_file_type = new QLabel("File Type :",this);
    line_edit_file_name = new QLineEdit(this);
    combo_box_file_type = new QComboBox(this);
    widget = new QWidget(this);
    layout_file_name = new QHBoxLayout();
    layout_file_type = new QHBoxLayout();
    layout_file_descriptor_button = new QHBoxLayout();
    layout_file_descriptor = new QVBoxLayout();
    layout_button = new QVBoxLayout();
    layout = new QVBoxLayout(widget);

    button_open_save->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_open_save->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_open_save->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    button_cancel->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_cancel->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_cancel->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    file_dialog_model->setReadOnly(true);
    tree_view->setModel(file_dialog_model);
    tree_view->setMinimumSize(TREE_VIEW_WIDTH,TREE_VIEW_HEIGHT);
    tree_view->setMaximumSize(TREE_VIEW_WIDTH,TREE_VIEW_HEIGHT);
    tree_view->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    QModelIndex index = file_dialog_model->index(PROJECT_PATH);
    tree_view->expand(index);
    tree_view->scrollTo(index);
    tree_view->setCurrentIndex(index);
    tree_view->resizeColumnToContents(0);

    line_edit_file_name->setMinimumSize(LINE_EDIT_WIDTH,LINE_EDIT_HEIGHT);
    line_edit_file_name->setMaximumSize(LINE_EDIT_WIDTH,LINE_EDIT_HEIGHT);
    line_edit_file_name->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    combo_box_file_type->addItem(".jconf");
    combo_box_file_type->setMinimumSize(COMBO_BOX_WIDTH,COMBO_BOX_HEIGHT);
    combo_box_file_type->setMaximumSize(COMBO_BOX_WIDTH,COMBO_BOX_HEIGHT);
    combo_box_file_type->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    layout_file_name->addWidget(label_file_name);
    layout_file_name->addWidget(line_edit_file_name);

    layout_file_type->addWidget(label_file_type);
    layout_file_type->addWidget(combo_box_file_type);

    layout_file_descriptor->addLayout(layout_file_name);
    layout_file_descriptor->addLayout(layout_file_type);

    layout_button->addWidget(button_open_save);
    layout_button->addWidget(button_cancel);

    connect(button_open_save,SIGNAL(clicked()),this,SLOT(on_Push_Button_Open_Save_clicked()));
    connect(button_cancel,SIGNAL(clicked()),this,SLOT(on_Push_Button_Cancel_clicked()));
    connect(tree_view,SIGNAL(clicked(QModelIndex)),this,SLOT(Update_Line_Edit_File_Name(QModelIndex)));
    connect(tree_view,SIGNAL(collapsed(QModelIndex)),this,SLOT(Update_Line_Edit_File_Name(QModelIndex)));
    connect(tree_view,SIGNAL(expanded(QModelIndex)),this,SLOT(Update_Line_Edit_File_Name(QModelIndex)));
    layout_file_descriptor_button->addLayout(layout_file_descriptor);
    layout_file_descriptor_button->addLayout(layout_button);

    layout->addWidget(tree_view);
    layout->addLayout(layout_file_descriptor_button);

    setMinimumSize(700,350);
    setMaximumSize(700,350);
    setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
}

File_Dialog::~File_Dialog()
{
    delete file_dialog_model;
    delete tree_view;
    delete button_open_save;
    delete button_cancel;
    delete label_file_name;
    delete label_file_type;
    delete line_edit_file_name;
    delete combo_box_file_type;
    delete layout_file_name;
    delete layout_file_type;
    delete layout_file_descriptor;
    delete layout_button;
    delete layout_file_descriptor_button;
    delete layout;
    delete widget;
}

void File_Dialog::Set_Operation(QString operation_str)
{
    button_open_save->setText(operation_str);
    this->setWindowTitle(operation_str + " File");
}

void File_Dialog::on_Push_Button_Open_Save_clicked()
{
    QString str = "";
    QModelIndex index = tree_view->currentIndex();
    if (!index.isValid())
    {
        return;
    }
    if(file_dialog_model->fileInfo(index).isDir())
    {
        // Open The Directory
        tree_view->expand(index);
        tree_view->scrollTo(index);
        tree_view->setCurrentIndex(index);
        tree_view->resizeColumnToContents(0);
    }
    else
    {
        // Assuming it's a file, whether it's exists or not
        if(!line_edit_file_name->text().isEmpty())
        {
            if(line_edit_file_name->text().endsWith(".jconf"))
            {
                str = line_edit_file_name->text();
            }
            else
            {
                str = line_edit_file_name->text() + ".jconf";
            }

            if(file_dialog_model->fileInfo(index).isFile())
            {
                // Write to existing or non existing file, if folder is not known, it must be created manually
                if(button_open_save->text().contains("Open"))
                {
                    emit Push_Button_Open_Triggered();
                    emit Push_Button_Open_Triggered(str);
                }
                if(button_open_save->text().contains("Save"))
                {
                    emit Push_Button_Save_Triggered();
                    emit Push_Button_Save_Triggered(str);
                }
                close();
            }
        }
        else
        {
            // do nothing
        }
    }
}

void File_Dialog::on_Push_Button_Cancel_clicked()
{
    close();
}

void File_Dialog::Update_Line_Edit_File_Name(QModelIndex index)
{
    line_edit_file_name->setText(file_dialog_model->fileInfo(index).absoluteFilePath());
}
