#ifndef FILE_DIALOG_H
#define FILE_DIALOG_H

#include <QDirModel>
#include <QLayout>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QTreeView>
#include <QPushButton>
#include <QWidget>

#include <QDebug>

class File_Dialog : public QWidget
{
    Q_OBJECT
    
    public:
        explicit File_Dialog(QWidget *parent = 0);
        ~File_Dialog();
        void Set_Operation(QString operation_str);

    signals:
        void Push_Button_Open_Triggered();
        void Push_Button_Open_Triggered(QString file_name);
        void Push_Button_Save_Triggered();
        void Push_Button_Save_Triggered(QString file_name);

    private slots:
        void on_Push_Button_Open_Save_clicked();
        void on_Push_Button_Cancel_clicked();
        void Update_Line_Edit_File_Name(QModelIndex index);

    private:
        QDirModel *file_dialog_model;
        QTreeView *tree_view;
        QPushButton *button_open_save;
        QPushButton *button_cancel;
        QLabel *label_file_name;
        QLabel *label_file_type;
        QLineEdit *line_edit_file_name;
        QComboBox *combo_box_file_type;
        QHBoxLayout *layout_file_name;
        QHBoxLayout *layout_file_type;
        QHBoxLayout *layout_file_descriptor_button;
        QVBoxLayout *layout_file_descriptor;
        QVBoxLayout *layout_button;
        QVBoxLayout *layout;
        QWidget *widget;

        static const int BUTTON_HEIGHT = 20;
        static const int BUTTON_WIDTH = 60;
        static const int COMBO_BOX_HEIGHT = 20;
        static const int COMBO_BOX_WIDTH = 540;
        static const int LINE_EDIT_HEIGHT = 20;
        static const int LINE_EDIT_WIDTH = 540;
        static const int TREE_VIEW_HEIGHT = 280;
        static const int TREE_VIEW_WIDTH = 680;
};

#endif // FILE_DIALOG_H
