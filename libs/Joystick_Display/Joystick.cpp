#include "Joystick.h"
#include <QDebug>

Joystick::Joystick(int sampling_rate_ms)
{
    enable = false;
    sampling_rate = sampling_rate_ms;
    device_id = 0;
    is_prev_square_pressed = false;
    is_prev_cross_pressed = false;
    is_prev_circle_pressed = false;
    is_prev_triangle_pressed = false;
    is_prev_r1_pressed = false;
    is_prev_l1_pressed = false;
    is_prev_r2_pressed = false;
    is_prev_l2_pressed = false;
    is_prev_start_pressed = false;
    is_prev_select_pressed = false;
    is_prev_r3_pressed = false;
    is_prev_l3_pressed = false;

    square_id = 0;
    cross_id = 1;
    circle_id = 2;
    triangle_id = 3;
    r2_id = 4;
    r1_id = 5;
    l2_id = 6;
    l1_id = 7;
    select_id = 8;
    start_id = 9;
    l3_id = 10;
    r3_id = 11;

    Search_Device();

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(Update()));
}

Joystick::~Joystick()
{
    delete timer;
}
void Joystick::Search_Device()
{
    bool found_flag = false;
    device_id_list.clear();
    sf::Joystick::update();
    for(int i=0;i<sf::Joystick::Count;i++)
    {
        if(sf::Joystick::isConnected(i))
        {
            found_flag = true;
            device_id_list.push_back(QString::number(i));
        }
    }
    if(found_flag)
    {
        device_id = device_id_list.at(0).toInt();
        // How many buttons does joystick #0 support?
        // unsigned int buttons = sf::Joystick::getButtonCount(device_id);
        // Does joystick #0 define a X axis?
        // bool hasX = sf::Joystick::hasAxis(device_id, sf::Joystick::X);
    }
}

void Joystick::Set_Device_ID(int id)
{
    device_id = id;
}

void Joystick::Set_Enable(bool mode)
{
    if(sf::Joystick::isConnected(device_id))
    {
        if(mode)
        {
            if(!enable)
            {
                timer->start(sampling_rate);
            }
        }
        else
        {
            if(enable)
            {
                timer->stop();
            }
        }
        enable = mode;
    }
}

unsigned int Joystick::Get_Square_ID()
{
    return square_id;
}

void Joystick::Set_Square_ID(unsigned int id)
{
    square_id = id;
}

unsigned int Joystick::Get_Cross_ID()
{
    return cross_id;
}

void Joystick::Set_Cross_ID(unsigned int id)
{
    cross_id = id;
}

unsigned int Joystick::Get_Circle_ID()
{
    return circle_id;
}

void Joystick::Set_Circle_ID(unsigned int id)
{
    circle_id = id;
}

unsigned int Joystick::Get_Triangle_ID()
{
    return triangle_id;
}

void Joystick::Set_Triangle_ID(unsigned int id)
{
    triangle_id = id;
}

unsigned int Joystick::Get_L1_ID()
{
    return l1_id;
}

void Joystick::Set_L1_ID(unsigned int id)
{
    l1_id = id;
}

unsigned int Joystick::Get_R1_ID()
{
    return r1_id;
}

void Joystick::Set_R1_ID(unsigned int id)
{
    r1_id = id;
}

unsigned int Joystick::Get_L2_ID()
{
    return l2_id;
}

void Joystick::Set_L2_ID(unsigned int id)
{
    l2_id = id;
}

unsigned int Joystick::Get_R2_ID()
{
    return r2_id;
}

void Joystick::Set_R2_ID(unsigned int id)
{
    r2_id = id;
}

unsigned int Joystick::Get_Select_ID()
{
    return select_id;
}

void Joystick::Set_Select_ID(unsigned int id)
{
    select_id = id;
}

unsigned int Joystick::Get_Start_ID()
{
    return start_id;
}

void Joystick::Set_Start_ID(unsigned int id)
{
    start_id = id;
}

unsigned int Joystick::Get_L3_ID()
{
    return l3_id;
}

void Joystick::Set_L3_ID(unsigned int id)
{
    l3_id = id;
}

unsigned int Joystick::Get_R3_ID()
{
    return r3_id;
}

void Joystick::Set_R3_ID(unsigned int id)
{
    r3_id = id;
}

void Joystick::Update()
{
    int left_analog_x;
    int left_analog_y;
    int right_analog_x;
    int right_analog_y;
    QString connect_status_str;
    sf::Joystick::update();
    if(sf::Joystick::isConnected(device_id))
    {
        connect_status_str = "Connected";
    }
    else
    {
        connect_status_str = "Disconnected";
    }
    left_analog_x = static_cast<int>(sf::Joystick::getAxisPosition(device_id, sf::Joystick::X) * 100);
    left_analog_y = static_cast<int>(sf::Joystick::getAxisPosition(device_id, sf::Joystick::Y) * -100);
    right_analog_x = static_cast<int>(sf::Joystick::getAxisPosition(device_id, sf::Joystick::Z) * 100);
    right_analog_y = static_cast<int>(sf::Joystick::getAxisPosition(device_id, sf::Joystick::R) * -100);
    emit Connect_Status_Received(connect_status_str);
    emit Button_Square_Received(sf::Joystick::isButtonPressed(device_id, square_id) && !is_prev_square_pressed) ;
    emit Button_Cross_Received(sf::Joystick::isButtonPressed(device_id, cross_id) && !is_prev_cross_pressed);
    emit Button_Circle_Received(sf::Joystick::isButtonPressed(device_id, circle_id) && !is_prev_circle_pressed);
    emit Button_Triangle_Received(sf::Joystick::isButtonPressed(device_id, triangle_id) && !is_prev_triangle_pressed);
    emit Button_L1_Received(sf::Joystick::isButtonPressed(device_id, l1_id) && !is_prev_l1_pressed);
    emit Button_R1_Received(sf::Joystick::isButtonPressed(device_id, r1_id) && !is_prev_r1_pressed);
    emit Button_L2_Received(sf::Joystick::isButtonPressed(device_id, l2_id) && !is_prev_l2_pressed);
    emit Button_R2_Received(sf::Joystick::isButtonPressed(device_id, r2_id) && !is_prev_r2_pressed);
    emit Button_Select_Received(sf::Joystick::isButtonPressed(device_id, select_id) && !is_prev_select_pressed);
    emit Button_Start_Received(sf::Joystick::isButtonPressed(device_id, start_id) && !is_prev_start_pressed);
    emit Button_L3_Received(sf::Joystick::isButtonPressed(device_id, l3_id) && !is_prev_l3_pressed);
    emit Button_R3_Received(sf::Joystick::isButtonPressed(device_id, r3_id) && !is_prev_r3_pressed);
    emit Left_Analog_Received(left_analog_x,left_analog_y);
    emit Right_Analog_Received(right_analog_x,right_analog_y);
    is_prev_square_pressed = sf::Joystick::isButtonPressed(device_id, square_id);
    is_prev_cross_pressed = sf::Joystick::isButtonPressed(device_id, cross_id);
    is_prev_circle_pressed = sf::Joystick::isButtonPressed(device_id, circle_id);
    is_prev_triangle_pressed = sf::Joystick::isButtonPressed(device_id, triangle_id);
    is_prev_r1_pressed = sf::Joystick::isButtonPressed(device_id, r1_id);
    is_prev_l1_pressed = sf::Joystick::isButtonPressed(device_id, l1_id);
    is_prev_r2_pressed = sf::Joystick::isButtonPressed(device_id, r2_id);
    is_prev_l2_pressed = sf::Joystick::isButtonPressed(device_id, l2_id);
    is_prev_start_pressed = sf::Joystick::isButtonPressed(device_id, start_id);
    is_prev_select_pressed = sf::Joystick::isButtonPressed(device_id, select_id);
    is_prev_r3_pressed = sf::Joystick::isButtonPressed(device_id, r3_id);
    is_prev_l3_pressed = sf::Joystick::isButtonPressed(device_id, l3_id);
    //qDebug() << connected << buttons << hasX;
}

unsigned int Joystick::Get_Pressed_Button_ID()
{
    if(sf::Joystick::isButtonPressed(device_id, square_id))
    {
        return square_id;
    }
    else if(sf::Joystick::isButtonPressed(device_id, cross_id))
    {
        return cross_id;
    }
    else if(sf::Joystick::isButtonPressed(device_id, circle_id))
    {
        return circle_id;
    }
    else if(sf::Joystick::isButtonPressed(device_id, triangle_id))
    {
        return triangle_id;
    }
    else if(sf::Joystick::isButtonPressed(device_id, l1_id))
    {
        return l1_id;
    }
    else if(sf::Joystick::isButtonPressed(device_id, r1_id))
    {
        return r1_id;
    }
    else if(sf::Joystick::isButtonPressed(device_id, l2_id))
    {
        return l2_id;
    }
    else if(sf::Joystick::isButtonPressed(device_id, r2_id))
    {
        return r2_id;
    }
    else if(sf::Joystick::isButtonPressed(device_id, select_id))
    {
        return select_id;
    }
    else if(sf::Joystick::isButtonPressed(device_id, start_id))
    {
        return start_id;
    }
    else if(sf::Joystick::isButtonPressed(device_id, l3_id))
    {
        return l3_id;
    }
    else if(sf::Joystick::isButtonPressed(device_id, r3_id))
    {
        return r3_id;
    }
    else
    {
        return 0;
    }
}
