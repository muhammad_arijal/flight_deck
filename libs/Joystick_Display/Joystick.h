#ifndef JOYSTICK_H
#define JOYSTICK_H

#include <QObject>
#include <QStringList>
#include <QTimer>
#include <SFML/Window/Joystick.hpp>

class Joystick : public QObject
{
    Q_OBJECT

	public:
        explicit Joystick(int sampling_rate_ms = 100);
    	~Joystick();
        void Search_Device();
        void Set_Device_ID(int id);
        void Set_Enable(bool mode);

        unsigned int Get_Square_ID();
        void Set_Square_ID(unsigned int id);
        unsigned int Get_Cross_ID();
        void Set_Cross_ID(unsigned int id);
        unsigned int Get_Circle_ID();
        void Set_Circle_ID(unsigned int id);
        unsigned int Get_Triangle_ID();
        void Set_Triangle_ID(unsigned int id);
        unsigned int Get_L1_ID();
        void Set_L1_ID(unsigned int id);
        unsigned int Get_R1_ID();
        void Set_R1_ID(unsigned int id);
        unsigned int Get_L2_ID();
        void Set_L2_ID(unsigned int id);
        unsigned int Get_R2_ID();
        void Set_R2_ID(unsigned int id);
        unsigned int Get_Select_ID();
        void Set_Select_ID(unsigned int id);
        unsigned int Get_Start_ID();
        void Set_Start_ID(unsigned int id);
        unsigned int Get_L3_ID();
        void Set_L3_ID(unsigned int id);
        unsigned int Get_R3_ID();
        void Set_R3_ID(unsigned int id);
        unsigned int Get_Pressed_Button_ID();

    public slots:
        void Update();

    signals:
        void Button_Square_Received(bool mode);
        void Button_Cross_Received(bool mode);
        void Button_Circle_Received(bool mode);
        void Button_Triangle_Received(bool mode);
        void Button_L1_Received(bool mode);
        void Button_R1_Received(bool mode);
        void Button_L2_Received(bool mode);
        void Button_R2_Received(bool mode);
        void Button_Select_Received(bool mode);
        void Button_Start_Received(bool mode);
        void Button_L3_Received(bool mode);
        void Button_R3_Received(bool mode);
        void Left_Analog_Received(int x, int y);
        void Right_Analog_Received(int x, int y);
        void Connect_Status_Received(QString str);

    private:        
        int device_id;
        bool is_prev_square_pressed;
        bool is_prev_cross_pressed;
        bool is_prev_circle_pressed;
        bool is_prev_triangle_pressed;
        bool is_prev_r1_pressed;
        bool is_prev_l1_pressed;
        bool is_prev_r2_pressed;
        bool is_prev_l2_pressed;
        bool is_prev_start_pressed;
        bool is_prev_select_pressed;
        bool is_prev_r3_pressed;
        bool is_prev_l3_pressed;
        unsigned int square_id;
        unsigned int cross_id;
        unsigned int circle_id;
        unsigned int triangle_id;
        unsigned int r2_id;
        unsigned int r1_id;
        unsigned int l2_id;
        unsigned int l1_id;
        unsigned int select_id;
        unsigned int start_id;
        unsigned int l3_id;
        unsigned int r3_id;
        int sampling_rate;
        QStringList device_id_list;
        QTimer *timer;
        bool enable;
};

#endif
