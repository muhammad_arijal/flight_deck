#include "Joystick_Display.h"

Joystick_Display::Joystick_Display(QWidget *parent, Joystick *joystick_arg) : QWidget(parent), joystick(joystick_arg)
{
    file_dialog = NULL;
    joystick_message_box = new Joystick_Message_Box();

    left_symbol = new QwtSymbol(QwtSymbol::XCross);
    right_symbol = new QwtSymbol(QwtSymbol::XCross);
    left_analog_display = new QwtPlot();
    right_analog_display = new QwtPlot();
    left_analog_plot_curve = new QwtPlotCurve();
    right_analog_plot_curve = new QwtPlotCurve();
    left_analog_samples = new QVector<QPointF>;
    right_analog_samples = new QVector<QPointF>;

    label_status = new QLabel("Not Connected",this);
    button_l1 = new QPushButton("L1",this);
    button_l2 = new QPushButton("L2",this);
    button_l3 = new QPushButton("L3",this);
    button_r1 = new QPushButton("R1",this);
    button_r2 = new QPushButton("R2",this);
    button_r3 = new QPushButton("R3",this);
    button_start = new QPushButton("Start",this);
    button_select = new QPushButton("Select",this);
    button_triangle = new QPushButton("Triangle",this);
    button_square = new QPushButton("Square",this);
    button_circle = new QPushButton("Circle",this);
    button_cross = new QPushButton("Cross",this);
    button_up = new QPushButton("Up",this);
    button_left = new QPushButton("Left",this);
    button_right = new QPushButton("Right",this);
    button_down = new QPushButton("Down",this);

    left_horizontal_spacer_upside_left = new QSpacerItem(40,20,QSizePolicy::Expanding,QSizePolicy::Expanding);
    left_horizontal_spacer_upside_right = new QSpacerItem(40,20,QSizePolicy::Expanding,QSizePolicy::Expanding);
    left_horizontal_spacer_center = new QSpacerItem(40,20,QSizePolicy::Expanding,QSizePolicy::Expanding);
    left_horizontal_spacer_bottom_left = new QSpacerItem(40,20,QSizePolicy::Expanding,QSizePolicy::Expanding);
    left_horizontal_spacer_bottom_right = new QSpacerItem(40,20,QSizePolicy::Expanding,QSizePolicy::Expanding);
    right_horizontal_spacer_upside_left = new QSpacerItem(40,20,QSizePolicy::Expanding,QSizePolicy::Expanding);
    right_horizontal_spacer_upside_right = new QSpacerItem(40,20,QSizePolicy::Expanding,QSizePolicy::Expanding);
    right_horizontal_spacer_center = new QSpacerItem(40,20,QSizePolicy::Expanding,QSizePolicy::Expanding);
    right_horizontal_spacer_bottom_left = new QSpacerItem(40,20,QSizePolicy::Expanding,QSizePolicy::Expanding);
    right_horizontal_spacer_bottom_right = new QSpacerItem(40,20,QSizePolicy::Expanding,QSizePolicy::Expanding);

    widget = new QWidget(this);
    layout_r1_l1 = new QHBoxLayout();
    layout_r2_l2 = new QHBoxLayout();
    layout_left_upside = new QHBoxLayout();
    layout_left_center = new QHBoxLayout();
    layout_left_bottom = new QHBoxLayout();
    layout_right_upside = new QHBoxLayout();
    layout_right_center = new QHBoxLayout();
    layout_right_bottom = new QHBoxLayout();
    layout_select_l3 = new QHBoxLayout();
    layout_start_r3 = new QHBoxLayout();
    layout_right = new QVBoxLayout();
    layout_left = new QVBoxLayout();
    layout_joystick = new QHBoxLayout();
    layout = new QVBoxLayout(widget);

    left_analog_display->enableAxis(QwtPlot::xBottom, false);
    left_analog_display->enableAxis(QwtPlot::yLeft, false);
    left_analog_display->setMinimumSize(35,35);
    left_analog_display->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    left_analog_display->setCanvasBackground(QColor(Qt::white));
    left_analog_display->setStyleSheet("background-color:white;");
    left_analog_display->setAxisScale(QwtPlot::yLeft, -10000.0, 10000.0);
    left_analog_display->setAxisScale(QwtPlot::xBottom, -10000.0, 10000.0);
    right_analog_display->enableAxis(QwtPlot::xBottom, false);
    right_analog_display->enableAxis(QwtPlot::yLeft, false);
    right_analog_display->setMinimumSize(35,35);
    right_analog_display->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    right_analog_display->setCanvasBackground(QColor(Qt::white));
    right_analog_display->setStyleSheet("background-color:white;");
    right_analog_display->setAxisScale(QwtPlot::yLeft, -10000.0, 10000.0);
    right_analog_display->setAxisScale(QwtPlot::xBottom, -10000.0, 10000.0);

    left_analog_samples->push_back(QPointF(0.0,0.0));
    right_analog_samples->push_back(QPointF(0.0,0.0));
    left_analog_plot_curve->setSamples(*left_analog_samples);
    right_analog_plot_curve->setSamples(*right_analog_samples);
    left_symbol->setColor(Qt::red);
    left_symbol->setSize(QSize(5,5));
    right_symbol->setColor(Qt::red);
    right_symbol->setSize(QSize(5,5));
    left_analog_plot_curve->setSymbol(left_symbol);
    right_analog_plot_curve->setSymbol(right_symbol);
    left_analog_plot_curve->attach(left_analog_display);
    right_analog_plot_curve->attach(right_analog_display);
    left_analog_display->replot();
    right_analog_display->replot();

    label_status->setAlignment(Qt::AlignCenter);
    label_status->setStyleSheet("color:yellow;");

    button_l1->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_l1->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_l1->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    button_l2->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_l2->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_l2->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    button_l3->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_l3->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_l3->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    button_r1->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_r1->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_r1->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    button_r2->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_r2->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_r2->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    button_r3->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_r3->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_r3->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    button_start->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_start->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_start->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    button_select->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_select->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_select->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    button_triangle->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_triangle->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_triangle->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    button_square->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_square->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_square->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    button_circle->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_circle->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_circle->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    button_cross->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_cross->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_cross->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    button_up->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_up->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_up->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    button_left->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_left->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_left->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    button_right->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_right->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_right->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    button_down->setMinimumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_down->setMaximumSize(BUTTON_WIDTH,BUTTON_HEIGHT);
    button_down->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    layout_r1_l1->addWidget(button_r1);
    layout_r1_l1->addWidget(button_l1);
    layout_right_upside->addItem(right_horizontal_spacer_upside_left);
    layout_right_upside->addWidget(button_triangle);
    layout_right_upside->addItem(right_horizontal_spacer_upside_right);
    layout_right_center->addWidget(button_square);
    layout_right_center->addItem(right_horizontal_spacer_center);
    layout_right_center->addWidget(button_circle);
    layout_right_bottom->addItem(right_horizontal_spacer_bottom_left);
    layout_right_bottom->addWidget(button_cross);
    layout_right_bottom->addItem(right_horizontal_spacer_bottom_right);
    layout_start_r3->addWidget(button_start);
    layout_start_r3->addWidget(button_r3);

    layout_right->addLayout(layout_r1_l1);
    layout_right->addLayout(layout_right_upside);
    layout_right->addLayout(layout_right_center);
    layout_right->addLayout(layout_right_bottom);
    layout_right->addLayout(layout_start_r3);

    layout_r2_l2->addWidget(button_l2);
    layout_r2_l2->addWidget(button_r2);
    layout_left_upside->addItem(left_horizontal_spacer_upside_left);
    layout_left_upside->addWidget(button_up);
    layout_left_upside->addItem(left_horizontal_spacer_upside_right);
    layout_left_center->addWidget(button_left);
    layout_left_center->addItem(left_horizontal_spacer_center);
    layout_left_center->addWidget(button_right);
    layout_left_bottom->addItem(left_horizontal_spacer_bottom_left);
    layout_left_bottom->addWidget(button_down);
    layout_left_bottom->addItem(left_horizontal_spacer_bottom_right);
    layout_select_l3->addWidget(button_l3);
    layout_select_l3->addWidget(button_select);

    layout_left->addLayout(layout_r2_l2);
    layout_left->addLayout(layout_left_upside);
    layout_left->addLayout(layout_left_center);
    layout_left->addLayout(layout_left_bottom);
    layout_left->addLayout(layout_select_l3);

    connect(joystick,SIGNAL(Left_Analog_Received(int,int)),this,SLOT(Update_Left_Analog(int,int)));
    connect(joystick,SIGNAL(Right_Analog_Received(int,int)),this,SLOT(Update_Right_Analog(int,int)));
    connect(joystick,SIGNAL(Button_Circle_Received(bool)),button_circle,SLOT(setDisabled(bool)));
    connect(joystick,SIGNAL(Button_Cross_Received(bool)),button_cross,SLOT(setDisabled(bool)));
    connect(joystick,SIGNAL(Button_L1_Received(bool)),button_l1,SLOT(setDisabled(bool)));
    connect(joystick,SIGNAL(Button_L2_Received(bool)),button_l2,SLOT(setDisabled(bool)));
    connect(joystick,SIGNAL(Button_L3_Received(bool)),button_l3,SLOT(setDisabled(bool)));
    connect(joystick,SIGNAL(Button_R1_Received(bool)),button_r1,SLOT(setDisabled(bool)));
    connect(joystick,SIGNAL(Button_R2_Received(bool)),button_r2,SLOT(setDisabled(bool)));
    connect(joystick,SIGNAL(Button_R3_Received(bool)),button_r3,SLOT(setDisabled(bool)));
    connect(joystick,SIGNAL(Button_Select_Received(bool)),button_select,SLOT(setDisabled(bool)));
    connect(joystick,SIGNAL(Button_Square_Received(bool)),button_square,SLOT(setDisabled(bool)));
    connect(joystick,SIGNAL(Button_Start_Received(bool)),button_start,SLOT(setDisabled(bool)));
    connect(joystick,SIGNAL(Button_Triangle_Received(bool)),button_triangle,SLOT(setDisabled(bool)));
    connect(joystick,SIGNAL(Connect_Status_Received(QString)),label_status,SLOT(setText(QString)));

    connect(joystick,SIGNAL(Button_Circle_Received(bool)),joystick_message_box,SLOT(Click_OK_Button(bool)));
    connect(joystick,SIGNAL(Button_Cross_Received(bool)),joystick_message_box,SLOT(Click_OK_Button(bool)));
    connect(joystick,SIGNAL(Button_L1_Received(bool)),joystick_message_box,SLOT(Click_OK_Button(bool)));
    connect(joystick,SIGNAL(Button_L2_Received(bool)),joystick_message_box,SLOT(Click_OK_Button(bool)));
    connect(joystick,SIGNAL(Button_L3_Received(bool)),joystick_message_box,SLOT(Click_OK_Button(bool)));
    connect(joystick,SIGNAL(Button_R1_Received(bool)),joystick_message_box,SLOT(Click_OK_Button(bool)));
    connect(joystick,SIGNAL(Button_R2_Received(bool)),joystick_message_box,SLOT(Click_OK_Button(bool)));
    connect(joystick,SIGNAL(Button_R3_Received(bool)),joystick_message_box,SLOT(Click_OK_Button(bool)));
    connect(joystick,SIGNAL(Button_Select_Received(bool)),joystick_message_box,SLOT(Click_OK_Button(bool)));
    connect(joystick,SIGNAL(Button_Square_Received(bool)),joystick_message_box,SLOT(Click_OK_Button(bool)));
    connect(joystick,SIGNAL(Button_Start_Received(bool)),joystick_message_box,SLOT(Click_OK_Button(bool)));
    connect(joystick,SIGNAL(Button_Triangle_Received(bool)),joystick_message_box,SLOT(Click_OK_Button(bool)));

    layout_joystick->addLayout(layout_left);
    layout_joystick->addWidget(left_analog_display);
    layout_joystick->addWidget(right_analog_display);
    layout_joystick->addLayout(layout_right);

    layout->addLayout(layout_joystick);
    layout->addWidget(label_status);

    setMinimumSize(450,200);
    setMaximumSize(450,200);
    setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
}

Joystick_Display::~Joystick_Display()
{
    delete joystick_message_box;
    delete left_analog_samples;
    delete right_analog_samples;
    delete left_analog_plot_curve;
    delete right_analog_plot_curve;
    delete left_analog_display;
    delete right_analog_display;
    delete label_status;
    delete button_l1;
    delete button_l2;
    delete button_l3;
    delete button_r1;
    delete button_r2;
    delete button_r3;
    delete button_start;
    delete button_select;
    delete button_triangle;
    delete button_square;
    delete button_circle;
    delete button_cross;
    delete button_up;
    delete button_left;
    delete button_right;
    delete button_down;
    delete layout_r1_l1;
    delete layout_r2_l2;
    delete layout_select_l3;
    delete layout_start_r3;
    delete layout_left_upside;
    delete layout_left_center;
    delete layout_left_bottom;
    delete layout_right_upside;
    delete layout_right_center;
    delete layout_right_bottom;
    delete layout_right;
    delete layout_left;
    delete layout_joystick;
    delete layout;
    delete widget;
}

void Joystick_Display::Create_Settings()
{
    bool is_continue = true;
    unsigned int square_id = 0;
    unsigned int cross_id = 0;
    unsigned int circle_id = 0;
    unsigned int triangle_id = 0;
    unsigned int r2_id = 0;
    unsigned int r1_id = 0;
    unsigned int l2_id = 0;
    unsigned int l1_id = 0;
    unsigned int select_id = 0;
    unsigned int start_id = 0;
    unsigned int l3_id = 0;
    unsigned int r3_id = 0;
    is_continue = Open_Message_Box_Press_Button("Square", is_continue);
    if(is_continue)
    {
        square_id = joystick->Get_Pressed_Button_ID();
    }
    is_continue = Open_Message_Box_Press_Button("Cross", is_continue);
    if(is_continue)
    {
        cross_id = joystick->Get_Pressed_Button_ID();
    }
    is_continue = Open_Message_Box_Press_Button("Circle", is_continue);
    if(is_continue)
    {
        circle_id = joystick->Get_Pressed_Button_ID();
    }
    is_continue = Open_Message_Box_Press_Button("Triangle", is_continue);
    if(is_continue)
    {
        triangle_id = joystick->Get_Pressed_Button_ID();
    }
    is_continue = Open_Message_Box_Press_Button("R1", is_continue);
    if(is_continue)
    {
        r1_id = joystick->Get_Pressed_Button_ID();
    }
    is_continue = Open_Message_Box_Press_Button("L1", is_continue);
    if(is_continue)
    {
        l1_id = joystick->Get_Pressed_Button_ID();
    }
    is_continue = Open_Message_Box_Press_Button("R2", is_continue);
    if(is_continue)
    {
        r2_id = joystick->Get_Pressed_Button_ID();
    }
    is_continue = Open_Message_Box_Press_Button("L2", is_continue);
    if(is_continue)
    {
        l2_id = joystick->Get_Pressed_Button_ID();
    }
    is_continue = Open_Message_Box_Press_Button("Start", is_continue);
    if(is_continue)
    {
        start_id = joystick->Get_Pressed_Button_ID();
    }
    is_continue = Open_Message_Box_Press_Button("Select", is_continue);
    if(is_continue)
    {
        select_id = joystick->Get_Pressed_Button_ID();
    }
    is_continue = Open_Message_Box_Press_Button("R3", is_continue);
    if(is_continue)
    {
        r3_id = joystick->Get_Pressed_Button_ID();
    }
    is_continue = Open_Message_Box_Press_Button("L3", is_continue);
    if(is_continue)
    {
        l3_id = joystick->Get_Pressed_Button_ID();
        joystick->Set_Square_ID(square_id);
        joystick->Set_Cross_ID(cross_id);
        joystick->Set_Circle_ID(circle_id);
        joystick->Set_Triangle_ID(triangle_id);
        joystick->Set_L1_ID(l1_id);
        joystick->Set_R1_ID(r1_id);
        joystick->Set_L2_ID(l2_id);
        joystick->Set_R2_ID(r2_id);
        joystick->Set_Select_ID(select_id);
        joystick->Set_Start_ID(start_id);
        joystick->Set_L3_ID(l3_id);
        joystick->Set_R3_ID(r3_id);
        if(file_dialog != NULL)
        {
            disconnect(file_dialog,SIGNAL(Push_Button_Save_Triggered(QString)),this,SLOT(Write_Settings(QString)));
            disconnect(file_dialog,SIGNAL(Push_Button_Open_Triggered(QString)),this,SLOT(Read_Settings(QString)));
            delete file_dialog;
        }
        file_dialog = new File_Dialog(0);
        connect(file_dialog,SIGNAL(Push_Button_Save_Triggered(QString)),this,SLOT(Write_Settings(QString)));
        connect(file_dialog,SIGNAL(Push_Button_Open_Triggered(QString)),this,SLOT(Read_Settings(QString)));
        file_dialog->Set_Operation("Save");
        file_dialog->show();
    }
}

void Joystick_Display::Load_Settings()
{
    if(file_dialog != NULL)
    {
        disconnect(file_dialog,SIGNAL(Push_Button_Save_Triggered(QString)),this,SLOT(Write_Settings(QString)));
        disconnect(file_dialog,SIGNAL(Push_Button_Open_Triggered(QString)),this,SLOT(Read_Settings(QString)));
        delete file_dialog;
    }
    file_dialog = new File_Dialog(0);
    connect(file_dialog,SIGNAL(Push_Button_Save_Triggered(QString)),this,SLOT(Write_Settings(QString)));
    connect(file_dialog,SIGNAL(Push_Button_Open_Triggered(QString)),this,SLOT(Read_Settings(QString)));
    file_dialog->Set_Operation("Open");
    file_dialog->show();
}

void Joystick_Display::Write_Settings(QString file_name)
{
    bool open_file_flag = true;
    QFile output_file(file_name);
    QTextStream output_stream(&output_file);
    output_file.open(QIODevice::WriteOnly);

    if (!output_file.isOpen())
    {
        qDebug() << output_file.fileName();
        qDebug() << "not found";
        open_file_flag = false;
    }

    if(open_file_flag)
    {
        output_stream << "Square = " << QString::number(joystick->Get_Square_ID()) << "\n";
        output_stream << "Cross = " << QString::number(joystick->Get_Cross_ID()) << "\n";
        output_stream << "Circle = " << QString::number(joystick->Get_Circle_ID()) << "\n";
        output_stream << "Triangle = " << QString::number(joystick->Get_Triangle_ID()) << "\n";
        output_stream << "R1 = " << QString::number(joystick->Get_R1_ID()) << "\n";
        output_stream << "L1 = " << QString::number(joystick->Get_L1_ID()) << "\n";
        output_stream << "R2 = " << QString::number(joystick->Get_R2_ID()) << "\n";
        output_stream << "L2 = " << QString::number(joystick->Get_L2_ID()) << "\n";
        output_stream << "Start = " << QString::number(joystick->Get_Start_ID()) << "\n";
        output_stream << "Select = " << QString::number(joystick->Get_Select_ID()) << "\n";
        output_stream << "R3 = " << QString::number(joystick->Get_R3_ID()) << "\n";
        output_stream << "L3 = " << QString::number(joystick->Get_L3_ID()) << "\n";
        output_file.close();
    }
}

void Joystick_Display::Read_Settings(QString file_name)
{
    bool open_file_flag = true;
    QFile input_file(file_name);
    QStringList line_stream_list;
    QTextStream input_stream(&input_file);
    input_file.open(QIODevice::ReadOnly | QIODevice::Text);

    if(!input_file.isOpen())
    {
        qDebug() << input_file.fileName();
        qDebug() << "not found";
        open_file_flag = false;
    }
    if(open_file_flag)
    {
        while(!input_stream.atEnd())
        {
            line_stream_list = input_stream.readLine().split(" ");
            if(line_stream_list.first().contains("Square"))
            {
                joystick->Set_Square_ID(line_stream_list.last().toUInt());
            }
            else if(line_stream_list.first().contains("Cross"))
            {
                joystick->Set_Cross_ID(line_stream_list.last().toUInt());
            }
            else if(line_stream_list.first().contains("Circle"))
            {
                joystick->Set_Circle_ID(line_stream_list.last().toUInt());
            }
            else if(line_stream_list.first().contains("Triangle"))
            {
                joystick->Set_Triangle_ID(line_stream_list.last().toUInt());
            }
            else if(line_stream_list.first().contains("R1"))
            {
                joystick->Set_R1_ID(line_stream_list.last().toUInt());
            }
            else if(line_stream_list.first().contains("L1"))
            {
                joystick->Set_L1_ID(line_stream_list.last().toUInt());
            }
            else if(line_stream_list.first().contains("R2"))
            {
                joystick->Set_R2_ID(line_stream_list.last().toUInt());
            }
            else if(line_stream_list.first().contains("L2"))
            {
                joystick->Set_L2_ID(line_stream_list.last().toUInt());
            }
            else if(line_stream_list.first().contains("Start"))
            {
                joystick->Set_Start_ID(line_stream_list.last().toUInt());
            }
            else if(line_stream_list.first().contains("Select"))
            {
                joystick->Set_Select_ID(line_stream_list.last().toUInt());
            }
            else if(line_stream_list.first().contains("R3"))
            {
                joystick->Set_R3_ID(line_stream_list.last().toUInt());
            }
            else if(line_stream_list.first().contains("L3"))
            {
                joystick->Set_L3_ID(line_stream_list.last().toUInt());
            }
            else
            {

            }
        }
        input_file.close();
    }
}

void Joystick_Display::Update_Left_Analog(int x, int y)
{
    left_analog_samples->clear();
    left_analog_samples->push_back(QPointF(static_cast<qreal>(x), static_cast<qreal>(y)));
    left_analog_plot_curve->setSamples(*left_analog_samples);
    left_analog_display->replot();
}

void Joystick_Display::Update_Right_Analog(int x, int y)
{
    right_analog_samples->clear();
    right_analog_samples->push_back(QPointF(static_cast<qreal>(x), static_cast<qreal>(y)));
    right_analog_plot_curve->setSamples(*right_analog_samples);
    right_analog_display->replot();
}

bool Joystick_Display::Open_Message_Box_Press_Button(QString button_str, bool mode)
{
    int retval;
    if(mode)
    {
        joystick_message_box->setIcon(QMessageBox::Question);
        joystick_message_box->setText("Press " + button_str + " Button!");
        retval = joystick_message_box->exec();
        if(retval == QMessageBox::Cancel)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
}
