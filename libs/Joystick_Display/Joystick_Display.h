#ifndef JOYSTICK_DISPLAY_H
#define JOYSTICK_DISPLAY_H

#include <QLabel>
#include <QLayout>
#include <QMessageBox>
#include <QPushButton>
#include <QStringList>
#include <QWidget>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_symbol.h>
#include <Joystick.h>
#include <Joystick_Message_Box.h>
#include <File_Dialog.h>

class Joystick_Display : public QWidget
{
    Q_OBJECT

    public:
        explicit Joystick_Display(QWidget *parent = 0, Joystick *joystick_arg = 0);
        void Create_Settings();
        void Load_Settings();
        ~Joystick_Display();

    private slots:
        void Update_Left_Analog(int x, int y);
        void Update_Right_Analog(int x, int y);
        void Write_Settings(QString file_name);
        void Read_Settings(QString file_name);

    private:
        QwtSymbol *left_symbol;
        QwtSymbol *right_symbol;
        QwtPlot *left_analog_display;
        QwtPlot *right_analog_display;
        QwtPlotCurve *left_analog_plot_curve;
        QwtPlotCurve *right_analog_plot_curve;
        QVector<QPointF>* left_analog_samples;
        QVector<QPointF>* right_analog_samples;
        Joystick *joystick;
        Joystick_Message_Box *joystick_message_box;
        File_Dialog *file_dialog;
        QLabel *label_status;
        QSpacerItem *left_horizontal_spacer_upside_left;
        QSpacerItem *left_horizontal_spacer_upside_right;
        QSpacerItem *left_horizontal_spacer_center;
        QSpacerItem *left_horizontal_spacer_bottom_left;
        QSpacerItem *left_horizontal_spacer_bottom_right;
        QSpacerItem *right_horizontal_spacer_upside_left;
        QSpacerItem *right_horizontal_spacer_upside_right;
        QSpacerItem *right_horizontal_spacer_center;
        QSpacerItem *right_horizontal_spacer_bottom_left;
        QSpacerItem *right_horizontal_spacer_bottom_right;
        QHBoxLayout *layout_r1_l1;
        QHBoxLayout *layout_r2_l2;
        QHBoxLayout *layout_select_l3;
        QHBoxLayout *layout_start_r3;
        QHBoxLayout *layout_left_upside;
        QHBoxLayout *layout_left_center;
        QHBoxLayout *layout_left_bottom;
        QHBoxLayout *layout_right_upside;
        QHBoxLayout *layout_right_center;
        QHBoxLayout *layout_right_bottom;
        QVBoxLayout *layout_right;
        QVBoxLayout *layout_left;
        QPushButton *button_l1;
        QPushButton *button_l2;
        QPushButton *button_l3;
        QPushButton *button_r1;
        QPushButton *button_r2;
        QPushButton *button_r3;
        QPushButton *button_start;
        QPushButton *button_select;
        QPushButton *button_triangle;
        QPushButton *button_square;
        QPushButton *button_circle;
        QPushButton *button_cross;
        QPushButton *button_up;
        QPushButton *button_left;
        QPushButton *button_right;
        QPushButton *button_down;
        QHBoxLayout *layout_joystick;
        QVBoxLayout *layout;
        QWidget *widget;

        static const int BUTTON_HEIGHT = 20;
        static const int BUTTON_WIDTH = 60;

        bool Open_Message_Box_Press_Button(QString button_str, bool mode);
};

#endif // JOYSTICK_DISPLAY_H
