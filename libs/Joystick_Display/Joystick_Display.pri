#################
# Main Source
#################
SOURCES     += 	$$PWD/Joystick_Display.cpp \
                $$PWD/Joystick_Message_Box.cpp \
                $$PWD/Joystick.cpp
HEADERS     += 	$$PWD/Joystick_Display.h \
                $$PWD/Joystick_Message_Box.h \
                $$PWD/Joystick.h
INCLUDEPATH += 	$$PWD
