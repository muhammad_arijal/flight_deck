#include "Joystick_Message_Box.h"

Joystick_Message_Box::Joystick_Message_Box(QDialog *parent) : QMessageBox(parent)
{
    button_ok = addButton(QMessageBox::Ok);
    button_cancel = addButton(QMessageBox::Cancel);
    button_ok->setHidden(true);
}

Joystick_Message_Box::~Joystick_Message_Box()
{
    delete button_ok;
    delete button_cancel;
}

void Joystick_Message_Box::Click_OK_Button(bool mode)
{
    if(mode)
    {
        button_ok->click();
    }
}
