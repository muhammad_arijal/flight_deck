#ifndef JOYSTICK_MESSAGE_BOX_H
#define JOYSTICK_MESSAGE_BOX_H

#include <QMessageBox>
#include <QPushButton>

class Joystick_Message_Box : public QMessageBox
{
    Q_OBJECT

    public:
        explicit Joystick_Message_Box(QDialog *parent = 0);
        ~Joystick_Message_Box();

    public slots:
        void Click_OK_Button(bool mode);

    private:
        QPushButton *button_ok;
        QPushButton *button_cancel;

};

#endif // JOYSTICK_MESSAGE_BOX_H
