#include "Main_Window.h"

Main_Window::Main_Window(QWidget *parent) : QMainWindow(parent)
{
    // System Construction
    task_serial_communication = new Task_Serial_Communication();
    locator_map = new Open_Street_Map(this);
    battery_status_display = new Battery_Status_Display(this);
    altitude_meter_display = new Altitude_Meter_Display(this);
    compass_display = new Compass_Display(this);
    speedometer_display = new Speedometer_Display(this);
    vertical_speed_indicator_display = new Vertical_Speed_Indicator_Display(this);
    joystick = new Joystick(100);
    joystick_display = new Joystick_Display(0,joystick);
    terminal_console = new Terminal_Console(0);
    media_player = new Media_Player();
    // UI Construction
    action_exit = new QAction(tr("&Exit"), this);
    action_load_joystick_settings = new QAction(tr("&Load Joystick Settings"), this);
    action_create_joystick_settings = new QAction(tr("&Create Joystick Settings"), this);
    action_fullscreen = new QAction(tr("&Fullscreen"), this);
    central_widget = new QWidget(this);
    hboxlayout_main = new QHBoxLayout();
    vboxlayout_compass_altitude = new QVBoxLayout();
    vboxlayout_vsi_speedometer = new QVBoxLayout();
    vboxlayout_battery_map_josytick = new QVBoxLayout();
    vboxlayout_mplayer_terminal = new QVBoxLayout();

    // Replace the file with the one you want to play
    media_player->playFile("bin/asd.mp4");

    // UI Layout
    menu_file = menuBar()->addMenu(tr("&File"));
    menu_file->addAction(action_create_joystick_settings);
    menu_file->addAction(action_load_joystick_settings);
    menu_file->addSeparator();
    menu_file->addAction(action_exit);

    action_fullscreen->setCheckable(true);
    menu_view = menuBar()->addMenu(tr("&View"));
    menu_view->addAction(action_fullscreen);

    central_widget->setLayout(hboxlayout_main);
    vboxlayout_vsi_speedometer->addWidget(speedometer_display);
    vboxlayout_vsi_speedometer->addWidget(vertical_speed_indicator_display);
    vboxlayout_compass_altitude->addWidget(compass_display);
    vboxlayout_compass_altitude->addWidget(altitude_meter_display);
    vboxlayout_battery_map_josytick->addWidget(battery_status_display);
    vboxlayout_battery_map_josytick->addWidget(locator_map);
    vboxlayout_battery_map_josytick->addWidget(joystick_display);
    vboxlayout_mplayer_terminal->addWidget(media_player);
    vboxlayout_mplayer_terminal->addWidget(terminal_console);
    hboxlayout_main->addLayout(vboxlayout_vsi_speedometer);
    hboxlayout_main->addLayout(vboxlayout_compass_altitude);
    hboxlayout_main->addLayout(vboxlayout_mplayer_terminal);
    hboxlayout_main->addLayout(vboxlayout_battery_map_josytick);

    connect(action_create_joystick_settings,SIGNAL(triggered()),this,SLOT(on_Action_Create_Joystick_Settings_triggered()));
    connect(action_load_joystick_settings,SIGNAL(triggered()),this,SLOT(on_Action_Load_Joystick_Settings_triggered()));
    connect(action_exit,SIGNAL(triggered()),this,SLOT(close()));
    connect(action_fullscreen,SIGNAL(triggered()),this,SLOT(on_Action_Full_Screen_triggered()));

    setCentralWidget(central_widget);
    setWindowTitle("Flight Deck v1.0");
    joystick->Set_Enable(true);
}

Main_Window::~Main_Window()
{
    delete task_serial_communication;
    delete battery_status_display;
    delete altitude_meter_display;
    delete compass_display;
    delete speedometer_display;
    delete vertical_speed_indicator_display;
    delete locator_map;
    delete media_player;
    delete joystick_display;
    delete joystick;
    delete menu_file;
    delete action_create_joystick_settings;
    delete action_load_joystick_settings;
    delete action_exit;
    delete menu_view;
    delete action_fullscreen;
    delete vboxlayout_vsi_speedometer;
    delete vboxlayout_compass_altitude;
    delete vboxlayout_mplayer_terminal;
    delete vboxlayout_battery_map_josytick;
    delete hboxlayout_main;
    delete central_widget;
}

void Main_Window::on_Action_Full_Screen_triggered()
{
    if (action_fullscreen->isChecked())
    {
        showFullScreen();
    }
    else
    {
        showNormal();
        showMaximized();
    }
}

void Main_Window::on_Action_Create_Joystick_Settings_triggered()
{
    joystick_display->Create_Settings();
}

void Main_Window::on_Action_Load_Joystick_Settings_triggered()
{
    joystick_display->Load_Settings();
}
