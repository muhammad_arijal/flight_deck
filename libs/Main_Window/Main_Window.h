#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QAction>
#include <QLayout>
#include <QMenu>
#include <QMenuBar>
#include <QMainWindow>
#include "Altitude_Meter_Display.h"
#include "Battery_Status_Display.h"
#include "Compass_Display.h"
#include "Open_Street_Map.h"
#include "Speedometer_Display.h"
#include "Vertical_Speed_Indicator_Display.h"
#include "Task_Serial_Communication.h"
#include "Joystick.h"
#include "Joystick_Display.h"
#include "Terminal_Console.h"
#include "Media_Player.h"

class Main_Window : public QMainWindow
{
    Q_OBJECT
    
    public:
        explicit Main_Window(QWidget *parent = 0);
        ~Main_Window();
    
    private slots:
        void on_Action_Full_Screen_triggered();
        void on_Action_Create_Joystick_Settings_triggered();
        void on_Action_Load_Joystick_Settings_triggered();

    private:
        Task_Serial_Communication *task_serial_communication;
        Battery_Status_Display *battery_status_display;
        Altitude_Meter_Display *altitude_meter_display;
        Compass_Display *compass_display;
        Speedometer_Display *speedometer_display;
        Vertical_Speed_Indicator_Display *vertical_speed_indicator_display;
        Open_Street_Map *locator_map;
        Media_Player *media_player;

        QMenu *menu_file;
        QAction *action_exit;
        QAction *action_create_joystick_settings;
        QAction *action_load_joystick_settings;

        QMenu *menu_view;
        QAction *action_fullscreen;

        QWidget *central_widget;
        QHBoxLayout *hboxlayout_main;
        QVBoxLayout *vboxlayout_vsi_speedometer;
        QVBoxLayout *vboxlayout_compass_altitude;
        QVBoxLayout *vboxlayout_battery_map_josytick;
        QVBoxLayout *vboxlayout_mplayer_terminal;

        Joystick *joystick;
        Joystick_Display *joystick_display;

        Terminal_Console *terminal_console;

};

#endif // MAIN_WINDOW_H
