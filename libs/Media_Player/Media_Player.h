#ifndef VLC_ON_QT_H
#define VLC_ON_QT_H

#include <QWidget>
#include <vlc/vlc.h>

class QVBoxLayout;
class QPushButton;
class QTimer;
class QFrame;
class QSlider;

#define POSITION_RESOLUTION 10000

class Media_Player : public QWidget
{
    Q_OBJECT
    private:
        QSlider *_positionSlider;
        QSlider *_volumeSlider;
        QFrame *_videoWidget;
        QTimer *poller;
        bool _isPlaying;
        //libvlc_exception_t _vlcexcep; // [20101215 JG] Used for versions prior to VLC 1.2.0.
        libvlc_instance_t *_vlcinstance;
        libvlc_media_player_t *_mp;
        libvlc_media_t *_m;

    public:
        Media_Player();
        ~Media_Player();
        //void raise(libvlc_exception_t * ex); // [20101215 JG] Used for versions prior to VLC 1.2.0.

    public slots:
        void playFile(QString file);
        void updateInterface();
        void changeVolume(int newVolume);
        void changePosition(int newPosition);

};
#endif
