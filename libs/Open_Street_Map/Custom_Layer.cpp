#include "Custom_Layer.h"

Custom_Layer::Custom_Layer(MarbleWidget* widget, Route *route, Heading *heading) : m_heading(heading), m_index(0), m_widget(widget), m_route(route)
{

}

Custom_Layer::~Custom_Layer()
{

}

QStringList Custom_Layer::renderPosition() const
{
    // We will paint in exactly one of the following layers. The current one can be changed by pressing the '+' key
    QStringList layers = QStringList() << "SURFACE" << "HOVERS_ABOVE_SURFACE";
    layers << "ORBIT" << "USER_TOOLS" << "STARS";
    int index = m_index % layers.size();
    return QStringList() << layers.at(index);
}

bool Custom_Layer::render(GeoPainter *painter, ViewportParams *viewport, const QString &renderPos, GeoSceneLayer *layer)
{
    Q_UNUSED(viewport);
    Q_UNUSED(renderPos);
    Q_UNUSED(layer);
    QImage image;
    qreal latitude;
    qreal longitude;
    GeoDataLineString line_string = m_route->Get_Checkpoint();
    image.load(":/images/images/placemark.png");
    painter->setRenderHint(QPainter::Antialiasing, true);
    if (!line_string.isEmpty())
    {
        for (int i = 0; i < line_string.size(); i++)
        {
            latitude = line_string.at(i).latitude(GeoDataCoordinates::Degree);
            longitude = line_string.at(i).longitude(GeoDataCoordinates::Degree);
            painter->drawImage(GeoDataCoordinates(longitude, latitude, 0, GeoDataCoordinates::Degree),image);
        }
    }
    return true;
}
