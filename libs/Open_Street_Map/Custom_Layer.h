#ifndef CUSTOM_LAYER_H
#define CUSTOM_LAYER_H

#include <QDebug>
#include <QKeyEvent>
#include <QObject>
#include <marble/GeoPainter.h>
#include <marble/LayerInterface.h>
#include <marble/MarbleMap.h>
#include <marble/MarbleModel.h>
#include <marble/MarbleWidget.h>
#include "Heading.h"
#include "Route.h"

using namespace Marble;

class Custom_Layer : public QObject, public LayerInterface
{
    Q_OBJECT

    public:
        explicit Custom_Layer(MarbleWidget* widget = 0, Route *route = 0, Heading *heading = 0);
        ~Custom_Layer();
        virtual QStringList renderPosition() const;
        virtual bool render(GeoPainter *painter, ViewportParams *viewport, const QString &renderPos = "NONE", GeoSceneLayer *layer = 0);        

    
    private:
        Heading *m_heading;
        int m_index;
        MarbleWidget *m_widget;
        Route *m_route;
};

#endif // CUSTOM_LAYER_H
