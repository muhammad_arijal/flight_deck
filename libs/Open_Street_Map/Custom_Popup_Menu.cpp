#include "Custom_Popup_Menu.h"

Custom_Popup_Menu::Custom_Popup_Menu(MarbleWidget *widget) : marble_widget(widget)
{
    menu = new QMenu();
    action_set_home = new QAction("Set Home", this);
    action_add_checkpoint = new QAction("Add Checkpoint", this);
    action_remove_last_checkpoint = new QAction("Remove Last Checkpoint", this);
    action_remove_all_checkpoint = new QAction("Remove All Checkpoint", this);

    menu->addAction(action_set_home);
    menu->addAction(action_add_checkpoint);
    menu->addAction(action_remove_last_checkpoint);
    menu->addAction(action_remove_all_checkpoint);

    connect(action_set_home, SIGNAL(triggered()), this, SLOT(Set_Home()));
    connect(action_add_checkpoint, SIGNAL(triggered()), this, SLOT(Add_Checkpoint()));
    connect(action_remove_last_checkpoint, SIGNAL(triggered()), this, SLOT(Remove_Last_Checkpoint()));
    connect(action_remove_all_checkpoint, SIGNAL(triggered()), this, SLOT(Remove_All_Checkpoint()));
}

Custom_Popup_Menu::~Custom_Popup_Menu()
{
    delete menu;
    delete action_set_home;
    delete action_add_checkpoint;
    delete action_remove_last_checkpoint;
    delete action_remove_all_checkpoint;
}

void Custom_Popup_Menu::Set_Data(QPoint coordinate)
{
    action_set_home->setData(coordinate);
    action_add_checkpoint->setData(coordinate);
    menu->popup(marble_widget->mapToGlobal(coordinate));
}

void Custom_Popup_Menu::Set_Home()
{
    emit Set_Home_Triggered(action_set_home->data().toPoint());
}

void Custom_Popup_Menu::Add_Checkpoint()
{
    emit Add_Checkpoint_Triggered(action_add_checkpoint->data().toPoint());
}

void Custom_Popup_Menu::Remove_Last_Checkpoint()
{
    emit Remove_Last_Checkpoint_Triggered();
}

void Custom_Popup_Menu::Remove_All_Checkpoint()
{
    emit Remove_All_Checkpoint_Triggered();
}
