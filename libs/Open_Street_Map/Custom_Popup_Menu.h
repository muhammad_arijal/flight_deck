#ifndef CUSTOM_POPUP_MENU_H
#define CUSTOM_POPUP_MENU_H

#include <QMenu>
#include <marble/MarbleWidget.h>

using namespace Marble;

class Custom_Popup_Menu : public QObject
{
    Q_OBJECT

    public:
        explicit Custom_Popup_Menu(MarbleWidget *widget = 0);
        ~Custom_Popup_Menu();

    signals:
        void Set_Home_Triggered(QPoint coordinate);
        void Add_Checkpoint_Triggered(QPoint coordinate);
        void Remove_Last_Checkpoint_Triggered();
        void Remove_All_Checkpoint_Triggered();

    public slots:
        void Set_Data(QPoint coordinate);

    private slots:
        void Set_Home();
        void Add_Checkpoint();
        void Remove_Last_Checkpoint();
        void Remove_All_Checkpoint();

    private:
        MarbleWidget *marble_widget;
        QMenu *menu;
        QAction *action_set_home;
        QAction *action_add_checkpoint;
        QAction *action_remove_last_checkpoint;
        QAction *action_remove_all_checkpoint;
};

#endif // CUSTOM_POPUP_MENU_H
