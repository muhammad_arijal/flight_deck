#include "Heading.h"

Heading::Heading()
{
    checkpoint = new GeoDataLineString();
}

Heading::~Heading()
{
    delete checkpoint;
}

GeoDataLineString Heading::Get_Checkpoint()
{
    return (*checkpoint);
}

qreal Heading::Get_Heading_Angle()
{
    qreal angle;
    qreal numerator;
    qreal denumerator;

    if (!checkpoint->isEmpty())
    {
        if(checkpoint->size() < 2)
        {
            numerator = checkpoint->at(checkpoint->size() - 1).longitude(GeoDataCoordinates::Degree);
            denumerator = checkpoint->at(checkpoint->size() - 1).latitude(GeoDataCoordinates::Degree);
        }
        else
        {
            numerator = checkpoint->at(checkpoint->size() - 1).longitude(GeoDataCoordinates::Degree) - checkpoint->at(checkpoint->size() - 2).longitude(GeoDataCoordinates::Degree);
            denumerator = checkpoint->at(checkpoint->size() - 1).latitude(GeoDataCoordinates::Degree) - checkpoint->at(checkpoint->size() - 2).latitude(GeoDataCoordinates::Degree);
        }
        angle = numerator / denumerator;
    }
    else
    {
        angle = 0.0;
    }

    return angle;
}

void Heading::Set_Checkpoint(GeoDataCoordinates position)
{
    if (checkpoint->size() > CHECKPOINT_BUFFER_LENGTH)
    {
        checkpoint->erase(checkpoint->begin());
    }
    (*checkpoint) << position;
}


//void Heading::Set_Icon(GeoDataStyle &style, QString directory, qreal rotation_value)
//{
//    QImage image;
//    QTransform transformer;
//    GeoDataIconStyle icon_style;
//    image.load(directory);
//    if (rotation_value > 0)
//    {
//        transformer.rotate(rotation_value);
//        image = image.transformed(transformer);
//    }
//    icon_style.setIcon(image);
//    style.setIconStyle(icon_style);
//}
