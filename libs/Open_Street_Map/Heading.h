#ifndef HEADING_H
#define HEADING_H

#include <marble/GeoDataDocument.h>
#include <marble/GeoDataIconStyle.h>
#include <marble/GeoDataPlacemark.h>
#include <marble/GeoDataStyle.h>

#define CHECKPOINT_BUFFER_LENGTH 5

using namespace Marble;

class Heading
{
    public:
        Heading();
        ~Heading();
        GeoDataLineString Get_Checkpoint();
        qreal Get_Heading_Angle();
        void Set_Checkpoint(GeoDataCoordinates position);
        //void Set_Icon(GeoDataStyle &style, QString directory, qreal rotation_value);

    private:
        GeoDataLineString *checkpoint;
};

#endif // HEADING_H
