#include "Open_Street_Map.h"

Open_Street_Map::Open_Street_Map(QWidget *parent) : QWidget(parent)
{
    distance_layout_horizontal_spacer = new QSpacerItem(100,20,QSizePolicy::Expanding,QSizePolicy::Expanding);
    spin_box_latitude = new QDoubleSpinBox(this);
    spin_box_longitude = new QDoubleSpinBox(this);
    line_edit_distance = new QLineEdit(this);
    line_edit_latitude = new QLineEdit(this);
    line_edit_longitude = new QLineEdit(this);
    label_distance = new QLabel("Distance :",this);
    label_distance_metric = new QLabel("m",this);
    label_latitude = new QLabel("Latitude :",this);
    label_latitude_plus = new QLabel("+",this);
    label_longitude = new QLabel("Longitude :",this);
    label_longitude_plus = new QLabel("+",this);
    map_zoom_bar = new QScrollBar(this);
    map_frame = new QFrame(this);
    locator_map = new MarbleWidget();
    locator_map_model = new MarbleModel();
    heading = new Heading();
    route = new Route();
    custom_layer = new Custom_Layer(locator_map, route, heading);
    popup_menu_right_mouse_button = new Custom_Popup_Menu(locator_map);

    widget = new QWidget(this);
    distance_layout = new QHBoxLayout();
    latitude_longitude_layout = new QHBoxLayout();
    map_layout = new QVBoxLayout();
    layout = new QVBoxLayout(widget);

    // Create a Marble QWidget without a parent
//    MarbleDirs::setMarbleDataPath("C:/Program Files (x86)/marble/data/");
//#ifdef QT_DEBUG
//    MarbleDirs::setMarblePluginPath("C:/Program Files (x86)/marble/plugins/debug/");
//#else
//    MarbleDirs::setMarblePluginPath("C:/Program Files (x86)/marble/plugins/release/");
//#endif

    // Add Custom Layer for Route Checkpoint
    locator_map->addLayer(custom_layer);

    // Add the document to MarbleWidget's tree model
    locator_map->model()->treeModel()->addDocument(route->Get_Document());

    // Center the map onto a given position
    home.set(107.61, -6.9, 0.0, GeoDataCoordinates::Degree);
    locator_map->centerOn(home);

    // Load the Open Street Map map with Mercator Projection
    locator_map->setMapThemeId("earth/openstreetmap/openstreetmap.dgml");
    locator_map->setProjection(Mercator);
    locator_map->zoomView(map_zoom_bar->value());

    // Connect the zoom slider to the map widget and vice versa
    connect(map_zoom_bar, SIGNAL(valueChanged(int)), locator_map, SLOT(zoomView(int)));
    connect(locator_map, SIGNAL(zoomChanged(int)), map_zoom_bar, SLOT(setValue(int)));

    // Connect the map widget to updating the line edit for displaying latitude and longitude coordinates
    connect(locator_map, SIGNAL(mouseMoveGeoPosition(QString)), this, SLOT(Update_Map_Position(QString)));

    // Hide the FloatItems: OverviewMap, ScaleBar, Compass, and Cross Hair
    locator_map->setShowOverviewMap(false);
    locator_map->setShowScaleBar(false);
    locator_map->setShowCompass(false);
    locator_map->setShowCrosshairs(false);

    // Setting Custom Popup Menu, connecting signal and slot for Custom Popup Menu
    locator_map->inputHandler()->setMouseButtonPopupEnabled(Qt::LeftButton,false);
    locator_map->inputHandler()->setMouseButtonPopupEnabled(Qt::RightButton,false);
    locator_map->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(locator_map, SIGNAL(customContextMenuRequested(QPoint)), popup_menu_right_mouse_button, SLOT(Set_Data(QPoint)));
    connect(popup_menu_right_mouse_button, SIGNAL(Set_Home_Triggered(QPoint)), this, SLOT(Set_Home(QPoint)));
    connect(popup_menu_right_mouse_button, SIGNAL(Add_Checkpoint_Triggered(QPoint)), this, SLOT(Add_Checkpoint(QPoint)));
    connect(popup_menu_right_mouse_button, SIGNAL(Remove_Last_Checkpoint_Triggered()), this, SLOT(Remove_Last_Checkpoint()));
    connect(popup_menu_right_mouse_button, SIGNAL(Remove_All_Checkpoint_Triggered()), this, SLOT(Remove_All_Checkpoint()));

    line_edit_distance->setText("0");
    line_edit_distance->setMaximumSize(80,20);
    line_edit_distance->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    line_edit_latitude->setText("NaN");
    line_edit_latitude->setMaximumSize(80,20);
    line_edit_latitude->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    line_edit_longitude->setText("NaN");
    line_edit_longitude->setMaximumSize(80,20);
    line_edit_longitude->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    distance_layout->addWidget(label_distance);
    distance_layout->addWidget(line_edit_distance);
    distance_layout->addWidget(label_distance_metric);
    distance_layout->addItem(distance_layout_horizontal_spacer);

    latitude_longitude_layout->addWidget(label_latitude);
    latitude_longitude_layout->addWidget(line_edit_latitude);
    latitude_longitude_layout->addWidget(label_latitude_plus);
    latitude_longitude_layout->addWidget(spin_box_latitude);
    latitude_longitude_layout->addWidget(label_longitude);
    latitude_longitude_layout->addWidget(line_edit_longitude);
    latitude_longitude_layout->addWidget(label_longitude_plus);
    latitude_longitude_layout->addWidget(spin_box_longitude);

    map_zoom_bar->setOrientation(Qt::Horizontal);
    map_zoom_bar->setMinimum(MIN_VALUE);
    map_zoom_bar->setMaximum(MAX_VALUE);
    map_zoom_bar->setValue(INIT_VALUE);
    map_zoom_bar->setMinimumSize(420,0);
    map_zoom_bar->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    // Add all widgets to the vertical layout
    map_layout->addWidget(locator_map);
    map_frame->setLayout(map_layout);
    map_frame->setMinimumSize(420,300);
    map_frame->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    layout->addWidget(map_frame);
    layout->addWidget(map_zoom_bar);
    layout->addLayout(distance_layout);
    layout->addLayout(latitude_longitude_layout);

    setMinimumSize(450,400);
    setMaximumSize(450,400);
    setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
}

Open_Street_Map::~Open_Street_Map()
{
    delete spin_box_latitude;
    delete spin_box_longitude;
    delete label_distance;
    delete label_distance_metric;
    delete label_latitude;
    delete label_latitude_plus;
    delete label_longitude;
    delete label_longitude_plus;
    delete line_edit_distance;
    delete line_edit_latitude;
    delete line_edit_longitude;
    delete locator_map;
    delete locator_map_model;
    delete map_layout;
    delete map_frame;
    delete distance_layout;
    delete latitude_longitude_layout;
    delete map_zoom_bar;
    delete custom_layer;
    delete popup_menu_right_mouse_button;
    delete heading;
    delete route;
    delete layout;
    delete widget;
}

void Open_Street_Map::Update_Map_Position(QString text)
{
    QStringList temp_list;
    QString temp;
    QString longitude_text;
    QString latitude_text;
    qreal longitude_value = 0.0;
    qreal latitude_value = 0.0;

    temp_list = text.split(",");
    if (temp_list.size() == 2)
    {
        longitude_text = temp_list.at(0);
        latitude_text = temp_list.at(1);

        temp_list = longitude_text.split(QString::fromUtf8("°"));
        if (temp_list.size() == 2)
        {
            longitude_value = temp_list.at(0).toFloat();
            temp_list = temp_list.at(1).split("'");
            if (temp_list.size() == 2)
            {
                longitude_value += (qreal(temp_list.at(0).toFloat())/60);
                temp_list = temp_list.at(1).split(QRegExp("\""), QString::KeepEmptyParts);
            }
            if (temp_list.size() == 2)
            {
                longitude_value += (qreal(temp_list.at(0).toFloat())/3600);
                temp = temp_list.at(1);
            }
            else // temp_list just contains one splitting value
            {
                temp = temp_list.at(0);
            }
            if (temp == "E")
            {
                longitude_value *= 1;
            }
            else if (temp == "W")
            {
                longitude_value *= -1;
            }
            else
            {
                longitude_value *= 0;
            }
            longitude_text = QString::number(longitude_value,'g',10);
        }
        else
        {
            longitude_text = "NaN";
        }

        temp_list = latitude_text.split(QString::fromUtf8("°"));
        if (temp_list.size() == 2)
        {
            latitude_value = temp_list.at(0).toFloat();
            temp_list = temp_list.at(1).split("'");
            if (temp_list.size() == 2)
            {
                latitude_value += (qreal(temp_list.at(0).toFloat())/60);
                temp_list = temp_list.at(1).split(QRegExp("\""), QString::KeepEmptyParts);
            }
            if (temp_list.size() == 2)
            {
                latitude_value += (qreal(temp_list.at(0).toFloat())/3600);
                temp = temp_list.at(1);
            }
            else // temp_list just contains one splitting value
            {
                temp = temp_list.at(0);
            }
            if (temp == "N")
            {
                latitude_value *= 1;
            }
            else if (temp == "S")
            {
                latitude_value *= -1;
            }
            else
            {
                latitude_value *= 0;
            }
            latitude_text = QString::number(latitude_value,'g',10);
        }
        else
        {
            latitude_text = "NaN";
        }
    }
    else
    {
        longitude_text = "NaN";
        latitude_text = "NaN";
    }
    line_edit_longitude->setText(longitude_text);
    line_edit_longitude->setCursorPosition(0);
    line_edit_latitude->setText(latitude_text);
    line_edit_latitude->setCursorPosition(0);
}

void Open_Street_Map::Set_Home(QPoint coordinate)
{    
    qreal latitude;
    qreal longitude;

    locator_map->geoCoordinates(coordinate.x(), coordinate.y(), longitude, latitude, GeoDataCoordinates::Degree);
    home.set(longitude, latitude, 0.0, GeoDataCoordinates::Degree);
    locator_map->centerOn(home);
}

void Open_Street_Map::Add_Checkpoint(QPoint coordinate)
{    
    qreal latitude;
    qreal longitude;
    qreal total_distance;

    GeoDataLineString line_string = route->Get_Checkpoint();
    locator_map->geoCoordinates(coordinate.x(), coordinate.y(), longitude, latitude, GeoDataCoordinates::Degree);
    line_string << GeoDataCoordinates(longitude, latitude, 0, GeoDataCoordinates::Degree);
    route->Set_Checkpoint(line_string);
    locator_map->model()->treeModel()->removeDocument(route->Get_Document());
    locator_map->model()->treeModel()->addDocument(route->Get_Document());

    total_distance = line_string.length(locator_map_model->planet()->radius());
    if (total_distance >= 1000.0)
    {
        line_edit_distance->setText(QString::number(total_distance/1000.0,'g',10));
        label_distance_metric->setText("km");
    }
    else
    {
        line_edit_distance->setText(QString::number(total_distance,'g',10));
        label_distance_metric->setText("m");
    }
}

void Open_Street_Map::Remove_Last_Checkpoint()
{
    qreal total_distance;
    GeoDataLineString line_string = route->Get_Checkpoint();

    if (((int) line_string.size()) > 0)
    {
        line_string.remove(((int) line_string.size()) - 1);
        route->Set_Checkpoint(line_string);
        locator_map->model()->treeModel()->removeDocument(route->Get_Document());
        locator_map->model()->treeModel()->addDocument(route->Get_Document());

        total_distance = line_string.length(locator_map_model->planet()->radius());
        if (total_distance >= 1000.0)
        {
            line_edit_distance->setText(QString::number(total_distance/1000.0,'g',10));
            label_distance_metric->setText("km");
        }
        else
        {
            line_edit_distance->setText(QString::number(total_distance,'g',10));
            label_distance_metric->setText("m");
        }
    }
}

void Open_Street_Map::Remove_All_Checkpoint()
{
    qreal total_distance;
    GeoDataLineString line_string = route->Get_Checkpoint();

    line_string.clear();
    route->Set_Checkpoint(line_string);
    locator_map->model()->treeModel()->removeDocument(route->Get_Document());
    locator_map->model()->treeModel()->addDocument(route->Get_Document());

    total_distance = line_string.length(locator_map_model->planet()->radius());
    if (total_distance >= 1000.0)
    {
        line_edit_distance->setText(QString::number(total_distance/1000.0,'g',10));
        label_distance_metric->setText("km");
    }
    else
    {
        line_edit_distance->setText(QString::number(total_distance,'g',10));
        label_distance_metric->setText("m");
    }
}
