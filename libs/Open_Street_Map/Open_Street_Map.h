#ifndef OPEN_STREET_MAP_H
#define OPEN_STREET_MAP_H

#include <QDoubleSpinBox>
#include <QLayout>
#include <QLabel>
#include <QLineEdit>
#include <QScrollBar>
#include <QSpacerItem>
#include <QWidget>
#include <marble/GeoDataTreeModel.h>
#include <marble/MarbleDirs.h>
#include <marble/MarbleDebug.h>
#include <marble/MarbleModel.h>
#include <marble/MarbleWidget.h>
#include <marble/MarbleWidgetInputHandler.h>
#include <marble/Planet.h>
#include "Custom_Layer.h"
#include "Custom_Popup_Menu.h"
#include "Heading.h"
#include "Route.h"

using namespace Marble;

class Open_Street_Map : public QWidget
{
    Q_OBJECT
    
    public:
        explicit Open_Street_Map(QWidget *parent = 0);
        ~Open_Street_Map();

    private slots:
        void Update_Map_Position(QString text);
        void Set_Home(QPoint coordinate);
        void Add_Checkpoint(QPoint coordinate);
        void Remove_Last_Checkpoint();
        void Remove_All_Checkpoint();

    private:
        Custom_Layer *custom_layer;
        Custom_Popup_Menu *popup_menu_right_mouse_button;
        GeoDataCoordinates home;
        Heading *heading;
        MarbleWidget *locator_map;
        MarbleModel *locator_map_model;
        Route *route;
        QSpacerItem *distance_layout_horizontal_spacer;
        QDoubleSpinBox *spin_box_latitude;
        QDoubleSpinBox *spin_box_longitude;
        QLabel *label_distance;
        QLabel *label_distance_metric;
        QLabel *label_latitude;
        QLabel *label_latitude_plus;
        QLabel *label_longitude;
        QLabel *label_longitude_plus;
        QLineEdit *line_edit_distance;
        QLineEdit *line_edit_latitude;
        QLineEdit *line_edit_longitude;
        QFrame *map_frame;
        QHBoxLayout *distance_layout;
        QHBoxLayout *latitude_longitude_layout;
        QVBoxLayout *map_layout;
        QVBoxLayout *layout;
        QScrollBar *map_zoom_bar;
        QWidget *widget;
        static const int MIN_VALUE = 940;
        static const int MAX_VALUE = 3500;
        static const int INIT_VALUE = 2900;
};

#endif // OPEN_STREET_MAP_H
