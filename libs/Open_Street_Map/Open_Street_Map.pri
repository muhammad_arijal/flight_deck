#################
# Main Source
#################
SOURCES     += 	$$PWD/Open_Street_Map.cpp \
                $$PWD/Custom_Layer.cpp \
                $$PWD/Custom_Popup_Menu.cpp \
                $$PWD/Route.cpp \
                $$PWD/Heading.cpp
HEADERS     += 	$$PWD/Open_Street_Map.h \
                $$PWD/Custom_Layer.h \
                $$PWD/Custom_Popup_Menu.h \
                $$PWD/Route.h \
                $$PWD/Heading.h
INCLUDEPATH += 	$$PWD
