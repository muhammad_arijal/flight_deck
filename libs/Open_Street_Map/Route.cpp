#include "Route.h"

Route::Route()
{
    document =  new GeoDataDocument();
    checkpoint = new GeoDataLineString();
    placemark = new GeoDataPlacemark();
    style_checkpoint = new GeoDataStyle();
    style_track = new GeoDataStyle();

    // Set style_track line color, width, and transparency
    GeoDataLineStyle line_style(QColor(ROUTE_TRACK_RED, ROUTE_TRACK_GREEN, ROUTE_TRACK_BLUE, ROUTE_TRACK_ALPHA_CHANNEL));
    line_style.setWidth(ROUTE_TRACK_WIDTH);
    style_track->setLineStyle(line_style);

    placemark->setGeometry(checkpoint);
    placemark->setStyle(style_track);
    document->append(placemark);
}

Route::~Route()
{
    delete style_checkpoint;
    delete style_track;
}

GeoDataDocument* Route::Get_Document()
{
    return document;
}

GeoDataLineString Route::Get_Checkpoint()
{
    return (*checkpoint);
}

void Route::Set_Checkpoint(GeoDataLineString line_string)
{
    (*checkpoint) = line_string;
}
