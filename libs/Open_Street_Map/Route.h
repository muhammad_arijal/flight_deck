#ifndef ROUTE_H
#define ROUTE_H

#include <marble/GeoDataDocument.h>
#include <marble/GeoDataLineString.h>
#include <marble/GeoDataPlacemark.h>
#include <marble/GeoDataStyle.h>

#define ROUTE_TRACK_RED 255
#define ROUTE_TRACK_GREEN 0
#define ROUTE_TRACK_BLUE 0
#define ROUTE_TRACK_ALPHA_CHANNEL 200
#define ROUTE_TRACK_WIDTH 6

using namespace Marble;

class Route
{
    public:
        Route();
        ~Route();
        GeoDataDocument* Get_Document();
        GeoDataLineString Get_Checkpoint();
        void Set_Checkpoint(GeoDataLineString line_string);

    private:
        GeoDataDocument *document;
        GeoDataLineString *checkpoint;
        GeoDataPlacemark *placemark;
        GeoDataStyle *style_checkpoint;
        GeoDataStyle *style_track;
};

#endif // ROUTE_H
