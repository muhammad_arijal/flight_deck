#include "Speedometer_Display.h"

Speedometer_Display::Speedometer_Display(QWidget *parent) : QWidget(parent)
{
    QUrl url;

    speedometer_view = new QDeclarativeView;
    widget = new QWidget(this);
    horizontal_slider = new QSlider(widget);
    layout = new QVBoxLayout(widget);

    url.setUrl("qrc:/qml/qml/Speedometer/main.qml");
    speedometer_view->setSource(url);

    horizontal_slider->setOrientation(Qt::Horizontal);
    horizontal_slider->setMinimum(MIN_VALUE);
    horizontal_slider->setMaximum(MAX_VALUE);
    horizontal_slider->setValue(INIT_VALUE);

    layout->addWidget(speedometer_view);
    layout->addWidget(horizontal_slider);

    root_object = speedometer_view->rootObject();

    widget->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    setMinimumSize(200,230);
    setMaximumSize(200,230);
    setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    connect(horizontal_slider, SIGNAL(valueChanged(int)), this, SLOT(Set_Value(int)));
}

Speedometer_Display::~Speedometer_Display()
{
    delete root_object;
    delete layout;
    delete speedometer_view;
    delete horizontal_slider;
    delete widget;

}

void Speedometer_Display::Set_Value(int value)
{
    root_object->setProperty("value",value);
}
