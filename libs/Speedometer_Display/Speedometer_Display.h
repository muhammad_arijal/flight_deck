#ifndef SPEEDOMETER_DISPLAY_H
#define SPEEDOMETER_DISPLAY_H

#include <QGraphicsObject>
#include <QtDeclarative/QDeclarativeView>
#include <QLayout>
#include <QSlider>
#include <QWidget>

class Speedometer_Display : public QWidget
{
    Q_OBJECT

    public:
        explicit Speedometer_Display(QWidget *parent = 0);
        ~Speedometer_Display();

    private slots:
        void Set_Value(int value);

    private:
        QDeclarativeView *speedometer_view;
        QGraphicsObject *root_object;
        QSlider *horizontal_slider;
        QVBoxLayout *layout;
        QWidget *widget;
        static const int MIN_VALUE = 0;
        static const int MAX_VALUE = 6000;
        static const int INIT_VALUE = MIN_VALUE;
};

#endif // SPEEDOMETER_DISPLAY_H
