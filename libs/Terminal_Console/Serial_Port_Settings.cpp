#include "Serial_Port_Settings.h"

Serial_Port_Settings::Serial_Port_Settings()
{

}

void Serial_Port_Settings::Set_Port_Name(QString mode)
{
    port_name = mode;
}

void Serial_Port_Settings::Set_Baud_Rate(Baud_Rate mode)
{
    baud_rate = mode;
}

void Serial_Port_Settings::Set_Data_Bits(Data_Bits mode)
{
    data_bits = mode;
}

void Serial_Port_Settings::Set_Parity(Parity mode)
{
    parity = mode;
}

void Serial_Port_Settings::Set_Stop_Bits(Stop_Bits mode)
{
    stop_bits = mode;
}

void Serial_Port_Settings::Set_Flow_Control(Flow_Control mode)
{
    flow_control = mode;
}

QString Serial_Port_Settings::Get_Port_Name()
{
    return port_name;
}

Serial_Port_Settings::Baud_Rate Serial_Port_Settings::Get_Baud_Rate()
{
    return baud_rate;
}

Serial_Port_Settings::Data_Bits Serial_Port_Settings::Get_Data_Bits()
{
    return data_bits;
}

Serial_Port_Settings::Parity Serial_Port_Settings::Get_Parity()
{
    return parity;
}

Serial_Port_Settings::Stop_Bits Serial_Port_Settings::Get_Stop_Bits()
{
    return stop_bits;
}

Serial_Port_Settings::Flow_Control Serial_Port_Settings::Get_Flow_Control()
{
    return flow_control;
}
