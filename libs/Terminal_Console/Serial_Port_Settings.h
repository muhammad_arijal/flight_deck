#ifndef SERIAL_PORT_SETTINGS_H
#define SERIAL_PORT_SETTINGS_H

#include <QString>
#include <qglobal.h>
#include <QtSerialPort/QSerialPort>

class Serial_Port_Settings
{
    public:
        struct Baud_Rate
        {
            qint32 value;
            QString text;
        };
        struct Data_Bits
        {
            QSerialPort::DataBits value;
            QString text;
        };
        struct Parity
        {
            QSerialPort::Parity value;
            QString text;
        };
        struct Stop_Bits
        {
            QSerialPort::StopBits value;
            QString text;
        };
        struct Flow_Control
        {
            QSerialPort::FlowControl value;
            QString text;
        };
        Serial_Port_Settings();
        void Set_Port_Name(QString mode);
        void Set_Baud_Rate(Baud_Rate mode);
        void Set_Data_Bits(Data_Bits mode);
        void Set_Parity(Parity mode);
        void Set_Stop_Bits(Stop_Bits mode);
        void Set_Flow_Control(Flow_Control mode);
        QString Get_Port_Name();
        Baud_Rate Get_Baud_Rate();
        Data_Bits Get_Data_Bits();
        Parity Get_Parity();
        Stop_Bits Get_Stop_Bits();
        Flow_Control Get_Flow_Control();

    private:
        QString port_name;
        Baud_Rate baud_rate;
        Data_Bits data_bits;
        Stop_Bits stop_bits;
        Parity parity;
        Flow_Control flow_control;
};

#endif // SERIAL_PORT_SETTINGS_H
