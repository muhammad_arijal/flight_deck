#include "Terminal_Console.h"

Terminal_Console::Terminal_Console(QWidget *parent) : QWidget(parent)
{
    is_connection_established = false;

    system = new QSerialPort();
    int_validator = new QIntValidator(0, 4000000, this);
    serial_port_settings = new Serial_Port_Settings();
    combo_box_com_port = new QComboBox(this);
    combo_box_baud_rate = new QComboBox(this);
    combo_box_parity = new QComboBox(this);
    combo_box_data_bits = new QComboBox(this);
    combo_box_stop_bits = new QComboBox(this);
    combo_box_flow_control = new QComboBox(this);
    label_com_port = new QLabel("COM Port :",this);
    label_baud_rate = new QLabel("Baud Rate :",this);
    label_parity = new QLabel("Parity :",this);
    label_data_bits = new QLabel("Data Bits :",this);
    label_stop_bits = new QLabel("Stop Bits :",this);
    label_flow_control = new QLabel("Flow Control :",this);
    line_edit_command = new QLineEdit(this);
    console = new QPlainTextEdit(this);
    button_connect = new QPushButton("Connect",this);
    button_send_command = new QPushButton("Send",this);

    widget = new QWidget(this);
    command_layout = new QHBoxLayout();
    setting_layout = new QHBoxLayout();
    com_port_data_bits_layout = new QVBoxLayout();
    baud_rate_stop_bits_layout = new QVBoxLayout();
    parity_flow_control_layout = new QVBoxLayout();
    layout = new QVBoxLayout(widget);

    console->setMinimumSize(430,100);
    console->setMaximumSize(430,100);
    console->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        com_port << info.portName();
    }
    combo_box_com_port->addItems(com_port);

    // fill baud rate (is not the entire list of available values,
    // desired values??, add your independently)
    combo_box_baud_rate->addItem(QLatin1String("9600"), QSerialPort::Baud9600);
    combo_box_baud_rate->addItem(QLatin1String("19200"), QSerialPort::Baud19200);
    combo_box_baud_rate->addItem(QLatin1String("38400"), QSerialPort::Baud38400);
    combo_box_baud_rate->addItem(QLatin1String("115200"), QSerialPort::Baud115200);
    combo_box_baud_rate->setCurrentIndex(0);

    // fill parity
    combo_box_parity->addItem(QLatin1String("None"), QSerialPort::NoParity);
    combo_box_parity->addItem(QLatin1String("Even"), QSerialPort::EvenParity);
    combo_box_parity->addItem(QLatin1String("Odd"), QSerialPort::OddParity);
    combo_box_parity->addItem(QLatin1String("Mark"), QSerialPort::MarkParity);
    combo_box_parity->addItem(QLatin1String("Space"), QSerialPort::SpaceParity);
    combo_box_parity->setCurrentIndex(0);

    // fill data bits
    combo_box_data_bits->addItem(QLatin1String("5"), QSerialPort::Data5);
    combo_box_data_bits->addItem(QLatin1String("6"), QSerialPort::Data6);
    combo_box_data_bits->addItem(QLatin1String("7"), QSerialPort::Data7);
    combo_box_data_bits->addItem(QLatin1String("8"), QSerialPort::Data8);
    combo_box_data_bits->setCurrentIndex(3);

    // fill stop bits
    combo_box_stop_bits->addItem(QLatin1String("1"), QSerialPort::OneStop);
    #ifdef Q_OS_WIN
        combo_box_stop_bits->addItem(QLatin1String("1.5"), QSerialPort::OneAndHalfStop);
    #endif
    combo_box_stop_bits->addItem(QLatin1String("2"), QSerialPort::TwoStop);
    combo_box_stop_bits->setCurrentIndex(0);

    // fill flow control
    combo_box_flow_control->addItem(QLatin1String("None"), QSerialPort::NoFlowControl);
    combo_box_flow_control->addItem(QLatin1String("RTS/CTS"), QSerialPort::HardwareControl);
    combo_box_flow_control->addItem(QLatin1String("XON/XOFF"), QSerialPort::SoftwareControl);
    combo_box_flow_control->setCurrentIndex(0);

    button_connect->setText("Connect");
    button_send_command->setEnabled(is_connection_established);
    console->setReadOnly(!is_connection_established);
    line_edit_command->setReadOnly(!is_connection_established);
    console->insertPlainText("tes");
    line_edit_command->setText("tes");

    com_port_data_bits_layout->addWidget(label_com_port);
    com_port_data_bits_layout->addWidget(combo_box_com_port);

    baud_rate_stop_bits_layout->addWidget(label_baud_rate);
    baud_rate_stop_bits_layout->addWidget(combo_box_baud_rate);

    parity_flow_control_layout->addWidget(label_parity);
    parity_flow_control_layout->addWidget(combo_box_parity);

    com_port_data_bits_layout->addWidget(label_data_bits);
    com_port_data_bits_layout->addWidget(combo_box_data_bits);

    baud_rate_stop_bits_layout->addWidget(label_stop_bits);
    baud_rate_stop_bits_layout->addWidget(combo_box_stop_bits);

    parity_flow_control_layout->addWidget(label_flow_control);
    parity_flow_control_layout->addWidget(combo_box_flow_control);

    command_layout->addWidget(line_edit_command);
    command_layout->addWidget(button_send_command);

    setting_layout->addLayout(com_port_data_bits_layout);
    setting_layout->addLayout(baud_rate_stop_bits_layout);
    setting_layout->addLayout(parity_flow_control_layout);
    setting_layout->addWidget(button_connect);

    layout->addWidget(console);
    layout->addLayout(command_layout);
    layout->addLayout(setting_layout);

    connect(system,SIGNAL(readyRead()),this,SLOT(Update_Terminal_Console()));

    setMinimumSize(450,400);
    setMaximumSize(450,400);
    setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    setWindowTitle("Terminal Console");
}

Terminal_Console::~Terminal_Console()
{
    delete int_validator;
    delete system;
    delete serial_port_settings;
    delete combo_box_com_port;
    delete combo_box_baud_rate;
    delete combo_box_parity;
    delete combo_box_data_bits;
    delete combo_box_stop_bits;
    delete combo_box_flow_control;
    delete label_com_port;
    delete label_baud_rate;
    delete label_parity;
    delete label_data_bits;
    delete label_stop_bits;
    delete label_flow_control;
    delete line_edit_command;
    delete console;
    delete button_send_command;
    delete button_connect;
    delete command_layout;
    delete com_port_data_bits_layout;
    delete baud_rate_stop_bits_layout;
    delete parity_flow_control_layout;
    delete setting_layout;
    delete layout;
    delete widget;
}

void Terminal_Console::on_Push_Button_COM_clicked()
{
    Update_Serial_Port_Settings();
    if(!is_connection_established)
    {
        system->setPortName(serial_port_settings->Get_Port_Name());
        if (!system->isOpen())
        {
            if (system->open(QIODevice::ReadWrite))
            {
                if (system->setBaudRate(serial_port_settings->Get_Baud_Rate().value)
                 && system->setParity(serial_port_settings->Get_Parity().value)
                 && system->setDataBits(serial_port_settings->Get_Data_Bits().value)
                 && system->setStopBits(serial_port_settings->Get_Stop_Bits().value)
                 && system->setFlowControl(serial_port_settings->Get_Flow_Control().value))
                {
                    is_connection_established = !is_connection_established;
                    console->clear();
//                    ui->Status_Bar->showMessage("Connected to " + ui->Combo_Box_COM_Port->currentText());
                    button_connect->setText("Disconnect");
                }
                else
                {
                    system->close();
//                    ui->Status_Bar->showMessage("Error Opening Serial Port : " + system->errorString());
                }
            }
            else
            {
//                ui->Status_Bar->showMessage("Error Configuring Serial Port : " + system->errorString());
            }
        }
    }
    else
    {
        if (system->isOpen())
        {
            system->close();
            is_connection_established = !is_connection_established;
//            ui->Status_Bar->showMessage("Disconnected from " + ui->Combo_Box_COM_Port->currentText());
            button_connect->setText("Connect");
        }
    }
    button_send_command->setEnabled(is_connection_established);
    console->setReadOnly(!is_connection_established);
    line_edit_command->setReadOnly(!is_connection_established);
    combo_box_baud_rate->setEnabled(!is_connection_established);
    combo_box_com_port->setEnabled(!is_connection_established);
    combo_box_data_bits->setEnabled(!is_connection_established);
    combo_box_flow_control->setEnabled(!is_connection_established);
    combo_box_parity->setEnabled(!is_connection_established);
    combo_box_stop_bits->setEnabled(!is_connection_established);
}

void Terminal_Console::Update_Terminal_Console()
{
    QByteArray data = system->readAll();
    console->insertPlainText("Received : " + QString(data));
}

void Terminal_Console::Update_Serial_Port_Settings()
{
    Serial_Port_Settings::Baud_Rate baud_rate;
    Serial_Port_Settings::Parity parity;
    Serial_Port_Settings::Data_Bits data_bits;
    Serial_Port_Settings::Stop_Bits stop_bits;
    Serial_Port_Settings::Flow_Control flow_control;

    baud_rate.value = static_cast<QSerialPort::BaudRate>(combo_box_baud_rate->itemData(combo_box_baud_rate->currentIndex()).toInt());
    baud_rate.text = combo_box_baud_rate->currentText();
    parity.value = static_cast<QSerialPort::Parity>(combo_box_parity->itemData(combo_box_parity->currentIndex()).toInt());
    parity.text = combo_box_parity->currentText();
    data_bits.value = static_cast<QSerialPort::DataBits>(combo_box_data_bits->itemData(combo_box_data_bits->currentIndex()).toInt());
    data_bits.text = combo_box_data_bits->currentText();
    stop_bits.value = static_cast<QSerialPort::StopBits>(combo_box_stop_bits->itemData(combo_box_stop_bits->currentIndex()).toInt());
    stop_bits.text = combo_box_stop_bits->currentText();
    flow_control.value = static_cast<QSerialPort::FlowControl>(combo_box_flow_control->itemData(combo_box_flow_control->currentIndex()).toInt());
    flow_control.text = combo_box_flow_control->currentIndex();

    serial_port_settings->Set_Port_Name(combo_box_com_port->currentText());
    serial_port_settings->Set_Baud_Rate(baud_rate);
    serial_port_settings->Set_Parity(parity);
    serial_port_settings->Set_Data_Bits(data_bits);
    serial_port_settings->Set_Stop_Bits(stop_bits);
    serial_port_settings->Set_Flow_Control(flow_control);
}

void Terminal_Console::on_Push_Button_Send_clicked()
{
    system->write(line_edit_command->text().toLocal8Bit());
}
