#ifndef TERMINAL_CONSOLE_H
#define TERMINAL_CONSOLE_H

#include <QComboBox>
#include <QIntValidator>
#include <QLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QWidget>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include "Serial_Port_Settings.h"

class Terminal_Console : public QWidget
{
    Q_OBJECT

    public:
        explicit Terminal_Console(QWidget *parent = 0);
        ~Terminal_Console();

    private slots:
        void on_Push_Button_COM_clicked();
        void Update_Terminal_Console();
        void on_Push_Button_Send_clicked();

    private:
        void Update_Serial_Port_Settings();
        bool is_connection_established;
        QIntValidator *int_validator;
        QSerialPort *system;
        QSerialPortInfo system_info;
        QStringList com_port;

        Serial_Port_Settings *serial_port_settings;
        QComboBox *combo_box_com_port;
        QComboBox *combo_box_baud_rate;
        QComboBox *combo_box_parity;
        QComboBox *combo_box_data_bits;
        QComboBox *combo_box_stop_bits;
        QComboBox *combo_box_flow_control;
        QLabel *label_com_port;
        QLabel *label_baud_rate;
        QLabel *label_parity;
        QLabel *label_data_bits;
        QLabel *label_stop_bits;
        QLabel *label_flow_control;
        QLineEdit *line_edit_command;
        QPlainTextEdit *console;
        QPushButton *button_connect;
        QPushButton *button_send_command;
        QHBoxLayout *command_layout;
        QHBoxLayout *setting_layout;
        QVBoxLayout *com_port_data_bits_layout;
        QVBoxLayout *baud_rate_stop_bits_layout;
        QVBoxLayout *parity_flow_control_layout;
        QVBoxLayout *layout;
        QWidget *widget;
};

#endif // TERMINAL_CONSOLE_H
