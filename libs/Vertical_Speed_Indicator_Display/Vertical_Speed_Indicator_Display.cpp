#include "Vertical_Speed_Indicator_Display.h"

Vertical_Speed_Indicator_Display::Vertical_Speed_Indicator_Display(QWidget *parent) : QWidget(parent)
{
    QUrl url;

    vertical_speed_indicator_view = new QDeclarativeView;
    widget = new QWidget(this);
    horizontal_slider = new QSlider(widget);
    layout = new QVBoxLayout(widget);

    url.setUrl("qrc:/qml/qml/Vertical_Speed_Indicator/main.qml");
    vertical_speed_indicator_view->setSource(url);

    horizontal_slider->setOrientation(Qt::Horizontal);
    horizontal_slider->setMinimum(MIN_VALUE);
    horizontal_slider->setMaximum(MAX_VALUE);
    horizontal_slider->setValue(INIT_VALUE);

    layout->addWidget(vertical_speed_indicator_view);
    layout->addWidget(horizontal_slider);

    root_object = vertical_speed_indicator_view->rootObject();

    widget->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    setMinimumSize(200,230);
    setMaximumSize(200,230);
    setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    connect(horizontal_slider, SIGNAL(valueChanged(int)), this, SLOT(Set_Value(int)));
}

Vertical_Speed_Indicator_Display::~Vertical_Speed_Indicator_Display()
{
    delete root_object;
    delete layout;
    delete vertical_speed_indicator_view;
    delete horizontal_slider;
    delete widget;
}

void Vertical_Speed_Indicator_Display::Set_Value(int value)
{
    root_object->setProperty("value",value);
}
