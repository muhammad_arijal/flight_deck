#ifndef VERTICAL_SPEED_INDICATOR_DISPLAY_H
#define VERTICAL_SPEED_INDICATOR_DISPLAY_H

#include <QGraphicsObject>
#include <QtDeclarative/QDeclarativeView>
#include <QLayout>
#include <QSlider>
#include <QWidget>

class Vertical_Speed_Indicator_Display : public QWidget
{
    Q_OBJECT
    
    public:
        explicit Vertical_Speed_Indicator_Display(QWidget *parent = 0);
        ~Vertical_Speed_Indicator_Display();

    private slots:
        void Set_Value(int value);

    private:
        QDeclarativeView *vertical_speed_indicator_view;
        QGraphicsObject *root_object;
        QSlider *horizontal_slider;
        QVBoxLayout *layout;
        QWidget *widget;
        static const int MIN_VALUE = -1000;
        static const int MAX_VALUE = 1000;
        static const int INIT_VALUE = (MIN_VALUE + MAX_VALUE) / 2;
};

#endif // VERTICAL_SPEED_INDICATOR_DISPLAY_H
