// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item
{
    id: root
    property real value : 0
    x: 0
    y: 0
    width: 200; height: 200
    Image
    {
        x: 7
        y: 7
        width: 186
        height: 186
        anchors.centerIn: parent
        source: "Images/Background.png"
    }

    Image
    {
        id: needle
        x: 98; y: 19
        width: 4
        height: 80
        smooth: true
        source: "Images/Needle.png"
        transform: Rotation
        {
            id: needle_rotation
            origin.x: 2; origin.y: 81
            angle: Math.min(Math.max(0, (root.value * 0.36)), 360)
            Behavior on angle
            {
                SpringAnimation
                {
                    spring: 0.8
                    damping: 0.16
                }
            }
        }
    }

    Image
    {
        id: shaft
        x: 89
        y: 91
        width: 22
        height: 21
        anchors.verticalCenterOffset: 2
        anchors.horizontalCenterOffset: 0
        anchors.centerIn: parent
        source: "Images/Shaft.png"
    }    
}
