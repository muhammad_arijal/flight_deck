// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item
{
    id: root
    property real value : 0
    x: 0
    y: 0
    width: 200; height: 200
    Image
    {
        x: 7
        y: 7
        width: 186
        height: 185
        anchors.centerIn: parent
        source: "Images/Background.png"
        transform: Rotation
        {
            id: background_rotation
            origin.x: 93; origin.y: 94
            angle: Math.min(Math.max(-360, (-root.value * 0.1)), 0)
            Behavior on angle
            {
                SpringAnimation
                {
                    spring: 0.8
                    damping: 0.16
                }
            }
        }
    }

    Image
    {
        id: angle_meter
        x: 14
        y: 7
        width: 177
        height: 190
        anchors.verticalCenterOffset: 2
        anchors.horizontalCenterOffset: 3
        anchors.centerIn: parent
        source: "Images/Angle_Meter.png"
    }    
}
