// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle
{
    id : root
    width: 200
    height: 200
    color: "black"
    property int value: 0
    Compass
    {
        id: compass
        anchors.centerIn: parent
        value: root.value
        objectName: "compass"
    }
    MouseArea
    {
        anchors.fill: parent
    }
}
