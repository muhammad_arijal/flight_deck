// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item
{
    id: root
    property real value : 0
    x: 0
    y: 0
    width: 200; height: 200
    Image
    {
        x: 7
        y: 7
        width: 186
        height: 186
        anchors.centerIn: parent
        source: "Images/Background.png"
    }

    Image
    {
        id: needle
        x: 21; y: 99
        width: 80
        height: 4
        smooth: true
        source: "Images/Needle.png"
        transform: Rotation
        {
            id: needle_rotation
            origin.x: 81; origin.y: 2
            angle: Math.min(Math.max(-45, (root.value * 0.045) - 45), 225)
            Behavior on angle
            {
                SpringAnimation
                {
                    spring: 0.8
                    damping: 0.16
                }
            }
        }
    }

    Image
    {
        id: shaft
        x: 92
        y: 92
        width: 20
        height: 21
        anchors.verticalCenterOffset: 3
        anchors.horizontalCenterOffset: 2
        anchors.centerIn: parent
        source: "Images/Shaft.png"
    }
}
