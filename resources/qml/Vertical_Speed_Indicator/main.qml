// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle
{
    id : root
    width: 200
    height: 200
    color: "black"
    property int value: 0
    Vertical_Speed_Indicator
    {
        id: vertical_speed_indicator
        anchors.centerIn: parent
        value: root.value
        objectName: "vertical_speed_indicator"
    }
    MouseArea
    {
        anchors.fill: parent
    }
}
