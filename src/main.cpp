#include "Main_Window.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Main_Window main_window;
    QFile background_css(":/style_sheet/style_sheet/Theme.css");
    QString style_sheet_background;

    background_css.open(QFile::ReadOnly);
    style_sheet_background = background_css.readAll();
    background_css.close();

    main_window.setStyleSheet(style_sheet_background);
    main_window.showMaximized();

    return a.exec();
}
